//
//  Apis.swift
//  MtitCrew
//
//  Created by 코레일 on 2021/04/21.
//

import Foundation

func getSession(second: TimeInterval) -> URLSession {
    let config = URLSessionConfiguration.default
    config.timeoutIntervalForRequest = TimeInterval(second)
    return URLSession(configuration: config)
}

/*func apiTitleImages(strUrl: String, postData: String, success: @escaping ([TITLE_IMAGES]?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)
    
    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("apiTitleImages response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode([TITLE_IMAGES].self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}*/

func apiLogin(strUrl: String, postData: String, success: @escaping (LOGIN?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
    //print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)
    
    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("apiSmartLogin response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(LOGIN.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiGetPreSchedule(strUrl: String, postData: String, success: @escaping (PRE_SCHEDULE?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)
    
    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        guard let response_str = String(data: data, encoding: .utf8) else { return }
        print("apiGetPreSchedule response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(PRE_SCHEDULE.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiGetProject(strUrl: String, postData: String, success: @escaping (PROJECT_LIST?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)
    
    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("apiGetProject response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(PROJECT_LIST.self, from: data)
            success(decode)
            //let list = decode.list
            //let decode2 = try JSONDecoder().decode([PROJECT].self, from: decode.list)
            //success(decode2)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiSchedule(strUrl: String, postData: String, success: @escaping (TRAIN_SCHEDULE?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }

    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        guard let response_str = String(data: data, encoding: .utf8) else { return }
        
        print("/apiSchedule response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(TRAIN_SCHEDULE.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiCode(strUrl: String, postData: String, success: @escaping (CODE?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    //guard let url = URL(string: Config().getRootUrl() + "googleLogin2.php") else {
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
    //print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiCode response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(CODE.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiGetNotice(strUrl: String, postData: String, success: @escaping (NOTICE?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)

    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
    //print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        guard let response_str = String(data: data, encoding: .utf8) else { return }
        print("/apiGetNotice response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(NOTICE.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiGetSupply(strUrl: String, postData: String, success: @escaping (SUPPLY?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)

    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
    //print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiGetSupply response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(SUPPLY.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiSetSupply(strUrl: String, postData: String, success: @escaping (WEB_RESULT?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)

    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
    //print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiGetSupply response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiCarSeries(strUrl: String, postData: String, success: @escaping (CAR_SERIES?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)

    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
    //print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiCarSeries response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(CAR_SERIES.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiDriver(strUrl: String, postData: String, success: @escaping (DRIVERS?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)

    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
    //print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        guard let response_str = String(data: data, encoding: .utf8) else { return }
        print("/apiDriver response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(DRIVERS.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiContack(strUrl: String, postData: String, success: @escaping (CONTACT_LIST?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)
    
    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("apiContack response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(CONTACT_LIST.self, from: data)
            success(decode)
            //let list = decode.list
            //let decode2 = try JSONDecoder().decode([PROJECT].self, from: decode.list)
            //success(decode2)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiNextDia(strUrl: String, postData: String, success: @escaping (NEXT_DIA?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)
    
    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        guard let response_str = String(data: data, encoding: .utf8) else { return }
        print("apiNextDia response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(NEXT_DIA.self, from: data)
            success(decode)
            //let list = decode.list
            //let decode2 = try JSONDecoder().decode([PROJECT].self, from: decode.list)
            //success(decode2)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiCheckRadio(strUrl: String, postData: String, success: @escaping (CHECK_RADIO_LIST?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)
    
    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("apiCheckRadio response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(CHECK_RADIO_LIST.self, from: data)
            success(decode)
            //let list = decode.list
            //let decode2 = try JSONDecoder().decode([PROJECT].self, from: decode.list)
            //success(decode2)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiCheckEquipment(strUrl: String, postData: String, success: @escaping (CHECK_EQUIPMENT_LIST?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)
    
    //print(url)
    //print(postData)
    
    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        guard let response_str = String(data: data, encoding: .utf8) else { return }
        print("apiCheckEquipment response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(CHECK_EQUIPMENT_LIST.self, from: data)
            success(decode)
            //let list = decode.list
            //let decode2 = try JSONDecoder().decode([PROJECT].self, from: decode.list)
            //success(decode2)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiCarCheck(strUrl: String, postData: String, success: @escaping (CHECK_CAR_LIST?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }

    //print(url)
    //print(postData)
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiCarCheck response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(CHECK_CAR_LIST.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiCarFault(strUrl: String, postData: String, success: @escaping (CAR_FAULT_LIST?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }

    //print(url)
    //print(postData)
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiCarFault response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(CAR_FAULT_LIST.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiBroadCast(strUrl: String, postData: String, success: @escaping (BROADCAST_LIST?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }

    //print(url)
    //print(postData)
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiBroadCast response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(BROADCAST_LIST.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiGetInstruction(strUrl: String, postData: String, success: @escaping (INSTRUCTION_LIST?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }

    //print(url)
    //print(postData)
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiGetInstruction response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(INSTRUCTION_LIST.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiSetInstruction(strUrl: String, postData: String, success: @escaping (WEB_RESULT?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }

    //print(url)
    //print(postData)
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiSetInstruction response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiSetCheckRadio(strUrl: String, postData: String, success: @escaping (WEB_RESULT?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }

    //print(url)
    //print(postData)
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiSetCheckRadio response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiSetCarCheck(strUrl: String, postData: String, success: @escaping (WEB_RESULT?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }

    //print(url)
    //print(postData)
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiSetCarCheck response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiSetCarFault(strUrl: String, postData: String, success: @escaping (WEB_RESULT?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }

    //print(url)
    //print(postData)
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiSetCarCheck response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiSetBroadcast(strUrl: String, postData: String, success: @escaping (WEB_RESULT?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }

    //print(url)
    //print(postData)
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiSetCarCheck response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiSubmit(strUrl: String, postData: String, success: @escaping (WEB_RESULT?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
    //print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiGetSupply response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiCancelData(strUrl: String, postData: String, success: @escaping (WEB_RESULT?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
    //print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        //guard let response_str = String(data: data, encoding: .utf8) else { return }
        //print("/apiGetSupply response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiGetFirstClass(strUrl: String, postData: String, success: @escaping (FIRST_CLASS?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
    print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        guard let response_str = String(data: data, encoding: .utf8) else { return }
        print("/apiGetFirstClass response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(FIRST_CLASS.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
}

func apiSetFirstClass(strUrl: String, postData: String, success: @escaping (SET_FIRST?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
     print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        guard let response_str = String(data: data, encoding: .utf8) else { return }
        print("/apiSetFirstClass response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(SET_FIRST.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
    
}

func apiGetBroadcastMessage(strUrl: String, postData: String, success: @escaping (BROADCAST_MESSAGE?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
     print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        guard let response_str = String(data: data, encoding: .utf8) else { return }
        print("/apiSetFirstClass response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(BROADCAST_MESSAGE.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
    
}

func apiDestination(strUrl: String, postData: String, success: @escaping (TRAIN_STATION_LIST?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
     print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        guard let response_str = String(data: data, encoding: .utf8) else { return }
        print("/apiSetFirstClass response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(TRAIN_STATION_LIST.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
    
}

func apiGetDiagram(strUrl: String, postData: String, success: @escaping (DIAGRAM?) -> Void, failure: @escaping (String?) -> Void) {
    let session = getSession(second: 10)
    guard let url = URL(string: Constants().getServcerUrl() + strUrl) else {
        failure("url error")
        return
    }
    //print(url)
     print(postData)
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = postData.data(using: .utf8)

    session.dataTask(with: request) { (rData, _, _) in
        guard let data = rData else { return }
        guard let response_str = String(data: data, encoding: .utf8) else { return }
        print("/apiSetFirstClass response_str -> \(response_str)")
        do {
            let decode = try JSONDecoder().decode(DIAGRAM.self, from: data)
            success(decode)
        } catch { failure("JSONDecoder error -> \(error.localizedDescription)") }
    }.resume()
    
}


