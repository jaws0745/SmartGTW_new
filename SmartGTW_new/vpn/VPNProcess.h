
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface VPNProcess : NSObject

- (BOOL)isVPNConnected;
- (void)connectVPN;
- (void)disconnectVPN;
- (NSString *)checkVpnStatus;

@end

