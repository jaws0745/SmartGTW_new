
#include <arpa/inet.h>
#include <ifaddrs.h>
#include "VPNProcess.h"

@implementation NSString (NSString_Extended)

- (NSString *)URLEncode {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[self UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}
@end

@interface VPNProcess()
@end

@implementation VPNProcess
- (instancetype)init
{
    self = [super init];
    
    return self;
}

- (BOOL)isVPNConnected
{
    BOOL vpnConnected = NO;
    struct ifaddrs* interfaces = NULL;
    struct ifaddrs* temp_addr = NULL;
    
    // retrieve the current interfaces - returns 0 on success
    NSInteger success = getifaddrs(&interfaces);
    if (success == 0)
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL)
        {
            //NSLog(@"sa_family:%d", temp_addr->ifa_addr->sa_family);
            if(temp_addr->ifa_addr->sa_family == AF_INET || temp_addr->ifa_addr->sa_family == AF_INET6)
            {
                NSString* name = [NSString stringWithUTF8String:temp_addr->ifa_name];
                NSString* address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                if ([name rangeOfString:@"utun"].location != NSNotFound && ![address isEqualToString:@"0.0.0.0"]) {
                    vpnConnected = YES;
                    break;
                }
                //NSLog(@"name:%@, address:%@", name, address);
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    return vpnConnected;
}

- (void)connectVPN {
    NSDictionary* dic = @{
        @"opmode"   : @"2",
        @"action"   : @"connect",
        @"return"   : @"smartoffice",
        @"userid"   : @"204186_55",
        @"password" : @"kim4375**",
        };

    NSURLComponents* components = [NSURLComponents componentsWithString:@"axvpn://"];
    NSMutableArray *queryItems = [NSMutableArray array];
    for (NSString *key in dic) {
        [queryItems addObject:[NSURLQueryItem queryItemWithName:key value:dic[key]]];
    }
    components.queryItems = queryItems;
    
    NSString* URL = [[components URL] absoluteString];
    [self openURL:URL];
}

- (void)disconnectVPN {
    NSDictionary* dic = @{
        @"opmode"   : @"2",
        @"action"   : @"disconnect",
        @"return"   : @"smartoffice",

    };
    NSURLComponents* components = [NSURLComponents componentsWithString:@"axvpn://"];
    NSMutableArray *queryItems = [NSMutableArray array];
    for (NSString *key in dic) {
        [queryItems addObject:[NSURLQueryItem queryItemWithName:key value:dic[key]]];
    }
    components.queryItems = queryItems;
    
    NSString* URL = [[components URL] absoluteString];
    [self openURL:URL];
}


- (NSString *)checkVpnStatus {
    NSString *status = [self isVPNConnected] ? @"VPN 연결됨" : @"VPN 연결 안됨";
    NSLog(@"VPN : %@", [self isVPNConnected] ? @"연결됨" : @"연결 안됨");
    
    return status;
}

- (void)openURL:(NSString*)URL
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:URL]]) {
        if (@available(iOS 10.0, *)) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URL] options:@{} completionHandler:^(BOOL success) {
                if (success) {
                    NSLog(@"SUCCESS");
                }
                else {
                    NSLog(@"FAIL");
                }
            }];
        } else {
            NSLog(@"FAIL");
        }
    }
    else {
        NSLog(@"CAN NOT OPEN URL");
    }
}

@end
