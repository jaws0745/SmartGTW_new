//
//  InstructionView.swift
//  SmartGTW_new
//
//  Created by 코레일 on 2022/09/05.
//

import SwiftUI

struct InstructionView: View {
    let config = Config()
    let util = Utility()
    @StateObject private var getApi = RequestAPI.shared
    
    @State var showAlert: Bool = false
    @State var isShowing: Bool = false
    
    @State var alertMessage: String = ""
    
    var body: some View {
        ZStack {
            Color.white2.ignoresSafeArea()
            
            VStack {
                ZStack {
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color.black, lineWidth: 1)
                        .background(Color.white)
                    VStack {
                        ForEach(getApi.arrayOfInstruction, id:\.self) { result in
                            Text(result.DRC_ATCL_CONT ?? "")
                                .font(.custom("Spoqa Han Sans Neo Regular", size: 16))
                                .foregroundColor(Color.black)
                                .frame(alignment: .topLeading)
                                .padding(.top, 20)
                        }
                    }
                    
                }
                .frame(width: 312, height: 500)
                .padding(.top, 20)
                .cornerRadius(8)
                Spacer()
                Button(action: {
                    Task {
                        let results = await getApi.setInst()
                        let result = results[0].result
                        let descript = results[0].descript
                        
                        if result == "success" {
                            alertMessage = "지시사항을 확인하였습니다."
                        }
                        else {
                            alertMessage = descript
                        }
                        showAlert = true
                    }
                }) {
                    ZStack {
                        RoundedRectangle(cornerRadius: 8)
                            .fill(Color.brightBlue)
                            .frame(width: 312, height: 56)
                        Text("확인")
                            .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                            .foregroundColor(Color.white)
                    }
                    .padding(.bottom, 20)
                }
                .alert("알림", isPresented: $showAlert) {
                    Button("확인") {
                        
                    }
                } message: {
                    Text(alertMessage)
                }
            }
            ProgressView()
                .isHidden(getApi.loading ? false : true)
        }
        .navigationTitle("지시 및 전달사항")
        .onAppear() {
            Task {
                if getApi.arrayOfInstruction.count > 0 {
                    
                }
                else {
                    await getApi.getInst()
                }
            }
        }
    }
}

struct InstructionView_Previews: PreviewProvider {
    static var previews: some View {
        InstructionView()
    }
}
