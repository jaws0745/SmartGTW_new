//
//  GoToWorkView.swift
//  SmartGTW_new
//
//  Created by 코레일 on 2022/08/29.
//

import SwiftUI

struct GoToWorkView: View {
    let locationManager = LocationManager()
    
    @State var showAlert: Bool = false
    @State var alertMessage: String = ""
    @State var mylat: Double = 0.0
    @State var mylong: Double = 0.0
    @State var distance: Double = 0.0
    
    var body: some View {
        ZStack {
            Color.white2.ignoresSafeArea()
            VStack {
                Button(action: {
                    let distance = getDistance()
                    
                    if distance <= 500 {
                        alertMessage = "출무에 성공하였습니다."
                        showAlert = true
                    }
                    else {
                        alertMessage = "사업소와의 거리가 충분하지 않습니다.0\n 사업소와 500m 이내에서 재시도하시기 바랍니다."
                        showAlert = true
                    }
                    
                    print(distance)
                }) {
                    ZStack {
                        RoundedRectangle(cornerRadius: 8)
                            .background(Color.brightBlue)
                            .frame(width: 312, height: 56)
                        Text("출무하기")
                            .foregroundColor(Color.white)
                            .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                    }
                    
                }
                .cornerRadius(8)
                .alert("알림", isPresented: $showAlert) {
                    Button("확인") {

                    }
                } message: {
                    Text(alertMessage)
                }
                Text(String(mylat))
                Text(String(mylong))
                Text(String(distance))
            }
        }
        .navigationTitle("출무")
    }
    
    func getDistance() -> Double {
        locationManager.updateLocation()
        mylat = locationManager.latitude!
        mylong = locationManager.longitude!
        
        print(mylat)
        print(mylong)
        
        let lat = 36.334423480493314
        let long = 127.43542821210707
        
        let a = lat - mylat
        let b = long - mylong
        
        distance = sqrt(pow(a,2) + pow(b,2))
        
        
        return abs(distance) * 100000
    }
}

struct GoToWorkView_Previews: PreviewProvider {
    static var previews: some View {
        GoToWorkView()
    }
}
