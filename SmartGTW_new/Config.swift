//
//  Config.swift
//  MtitCrew
//
//  Created by 코레일 on 2021/04/21.
//

import Foundation

class Config {
    func setAccessKey(accessKey: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(accessKey, forKey: "accessKey")
        userDefaults.synchronize()
    }

    func getAccessKey() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "accessKey") ?? ""
    }
    
    func setUserId(userId: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(userId, forKey: "userId")
        userDefaults.synchronize()
    }

    func getUserId() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "userId") ?? ""
    }
    
    func setUserPw(userPw: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(userPw, forKey: "userPw")
        userDefaults.synchronize()
    }

    func getUserPw() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "userId") ?? ""
    }
    
    func setUserName(userName: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(userName, forKey: "userName")
        userDefaults.synchronize()
    }

    func getUserName() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "userName") ?? ""
    }
    
    func setPjtDt(pjtDt: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(pjtDt, forKey: "pjtDt")
        userDefaults.synchronize()
    }

    func getPjtDt() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "pjtDt") ?? "19000101"
    }
    
    func setBlgCd(blgCd: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(blgCd, forKey: "blgCd")
        userDefaults.synchronize()
    }

    func getBlgCd() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "blgCd") ?? ""
    }
    
    func setBlgNm(blgNm: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(blgNm, forKey: "blgNm")
        userDefaults.synchronize()
    }

    func getBlgNm() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "blgNm") ?? ""
    }
    
    func setCrewPlanCd(crewPlanCd: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(crewPlanCd, forKey: "crewPlanCd")
        userDefaults.synchronize()
    }

    func getCrewPlanCd() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "crewPlanCd") ?? ""
    }
    
    func setCrewPlanNm(crewPlanNm: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(crewPlanNm, forKey: "crewPlanNm")
        userDefaults.synchronize()
    }

    func getCrewPlanNm() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "crewPlanNm") ?? ""
    }
    
    func setDiaNo(diaNo: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(diaNo, forKey: "diaNo")
        userDefaults.synchronize()
    }

    func getDiaNo() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "diaNo") ?? ""
    }
    
    func setDiaDvCd(diaDvCd: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(diaDvCd, forKey: "diaDvCd")
        userDefaults.synchronize()
    }

    func getDiaDvCd() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "diaDvCd") ?? ""
    }
    
    func setDiaStartDate(diaDvCd: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(diaDvCd, forKey: "diaStartDate")
        userDefaults.synchronize()
    }

    func getDiaStartDate() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "diaStartDate") ?? ""
    }
    
    func setGWK_tm(gwk: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(gwk, forKey: "gwk")
        userDefaults.synchronize()
    }

    func getGWK_tm() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "gwk") ?? "0000"
    }
    
    func setLOIW_tm(loiw: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(loiw, forKey: "loiw")
        userDefaults.synchronize()
    }

    func getLOIW_tm() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "loiw") ?? "0000"
    }
    
    func setTotTm(totTm: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(totTm, forKey: "totTm")
        userDefaults.synchronize()
    }

    func getTotTm() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "totTm") ?? "1000"
    }
    
    func setTotDst(totDst: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(totDst, forKey: "totDst")
        userDefaults.synchronize()
    }

    func getTotDst() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "totDst") ?? "10000"
    }
    
    func setSelectedTrainNo(selectedTrainNo: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(selectedTrainNo, forKey: "selectedTrainNo")
        userDefaults.synchronize()
    }

    func getSelectedTrainNo() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "selectedTrainNo") ?? ""
    }

    func setWorkDays(workDays: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(workDays, forKey: "work_days")
        userDefaults.synchronize()
    }

    func getWorkDays() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "work_days") ?? ""
    }
 
    func setIsWorkday(workday: Bool) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(workday, forKey: "isworkday")
        userDefaults.synchronize()
    }

    func getIsWorkday() -> Bool{
        let userDefaults = UserDefaults.standard
        return userDefaults.bool(forKey: "isworkday")
    }
    
    func setDiaUrl(diaUrl: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(diaUrl, forKey: "diaUrl")
        userDefaults.synchronize()
    }

    func getDiaUrl() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "diaUrl") ?? ""
    }
    
    func setTrainText(trainText: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(trainText, forKey: "trainText")
        userDefaults.synchronize()
    }

    func getTrainText() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "trainText") ?? ""
    }
    
    func setTrainDate(trainDate: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(trainDate, forKey: "trainDate")
        userDefaults.synchronize()
    }

    func getTrainDate() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "trainDate") ?? ""
    }
    
    func setIsFirstLogin(firstLogin: Bool) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(firstLogin, forKey: "firstLogin")
        userDefaults.synchronize()
    }

    func getIsFirstLogin() -> Bool{
        let userDefaults = UserDefaults.standard
        return userDefaults.bool(forKey: "firstLogin")
    }
    
    func setSupplyKey(key: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(key, forKey: "key")
        userDefaults.synchronize()
    }

    func getSupplyKey() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "key") ?? ""
    }
    
    func setSupplyValue(value: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(value, forKey: value)
        userDefaults.synchronize()
    }

    func getSupplyValue() -> String{
        let userDefaults = UserDefaults.standard
        return userDefaults.string(forKey: "value") ?? ""
    }
}
