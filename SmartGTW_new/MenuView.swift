//
//  MenuView.swift
//  SmartGTW
//
//  Created by 코레일 on 2022/08/29.
//

import SwiftUI
import CoreLocation

struct MenuView: View {
    let util = Utility()
    let config = Config()
    let locationManager = LocationManager()
    @StateObject private var getApi = RequestAPI.shared
    
    @GestureState private var translation: CGFloat = 0
    var offset: CGFloat = 0
    
    @Binding var logoutActive: Bool
    
    @State var showAlert: Bool = false
    @State var showSettingAlert: Bool = false
    @State var showCrewInfoMenu: Bool = false
    @State var showEduMenu: Bool = false
    @State var isFirstLogin: Bool = false
    @State var isAgreeLocation: Bool = false
    
    @State var destination: String? = ""
    @State var url = URL(string: UIApplication.openSettingsURLString)
    
    var body: some View {
        ZStack {
            Color.white2.ignoresSafeArea()
            VStack {
                HStack {
                    Spacer()
                    Text("스마트 출무시스템")
                        .foregroundColor(Color.black)
                        .font(.custom("Spoqa Han Sans Neo Bold", size: 20))
                        .padding(.trailing, 26)
                }
                .padding(.top, 40)
                Image("group_3")
                    .resizable()
                    .frame(width: .infinity, height: 30)
                HStack {
                    NavigationLink(destination: GoToWorkView(), tag: "goToWork", selection: $destination) {
                        Button(action: {
                            if config.getIsWorkday() == true {
                                switch locationManager.authorizationStatus {
                                case .authorizedAlways, .authorizedWhenInUse:
                                    destination = "goToWork"
                                case .restricted, .notDetermined:
                                    // 아직 결정하지 않은 상태: 시스템 팝업 호출
                                    locationManager.requestPermission()
                                case .denied:
                                    // 거부: 설정 창으로 가서 권한을 변경하도록 유도
                                    showSettingAlert = true
                                @unknown default:
                                    return
                                }
                            }
                            else {
                                showAlert = true
                            }
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 12)
                                    .stroke(Color.silver, lineWidth: 2)
                                    .background(Color.white)
                                    .cornerRadius(12)
                                    .frame(width: 150, height: 150)
                                VStack {
                                    Image("main_img_01")
                                        .resizable()
                                        .frame(width: 142, height: 81)
                                    Text("출무")
                                        .font(.custom("Spoqa Han Sans Neo Regular", size: 18))
                                        .foregroundColor(Color.black)
                                        .padding(.top, 6)
                                }
                            }
                        }
                        .alert("알림", isPresented: $showAlert) {
                            Button("확인") {
                            }
                        } message: {
                            Text("금일 운행스케쥴이 없어 해당 메뉴를 실행할 수 없습니다.")
                        }
                        .alert("알림", isPresented: $showSettingAlert) {
                            Button("취소", role: .cancel) {
                            }
                            Button("확인") {
                                UIApplication.shared.open(url!)
                            }
                        } message: {
                            Text("위치정보동의를 하지 않을 경우, 모바일 출무 기능을 사용할 수 없습니다.\n설정에서 위치권한 활성화를 원하실 경우 확인 버튼을 눌러주세요.")
                        }
                    }
                    
                    Button(action: {
                        if config.getIsWorkday() == true {
                            showEduMenu = false
                            withAnimation {
                                showCrewInfoMenu.toggle()
                            }
                        }
                        else {
                            showAlert = true
                        }
                    }) {
                        ZStack {
                            RoundedRectangle(cornerRadius: 12)
                                .stroke(Color.silver, lineWidth: 2)
                                .background(Color.white)
                                .cornerRadius(12)
                                .frame(width: 150, height: 150)
                            VStack {
                                Image("main_img_02")
                                    .resizable()
                                    .frame(width: 142, height: 81)
                                Text("승무정보")
                                    .font(.custom("Spoqa Han Sans Neo Regular", size: 18))
                                    .foregroundColor(Color.black)
                                    .padding(.top, 6)
                            }
                        }
                        .padding(.leading, 10)
                    }
                    .alert("알림", isPresented: $showAlert) {
                        Button("확인") {
                        }
                    } message: {
                        Text("금일 운행스케쥴이 없어 해당 메뉴를 실행할 수 없습니다.")
                    }
                    
                }
                HStack {
                    NavigationLink(destination: EducationView(), tag: "education", selection: $destination) {
                        Button(action: {
                            if config.getIsWorkday() == true {
                                showCrewInfoMenu = false
                                withAnimation {
                                    showEduMenu.toggle()
                                }
                            }
                            else {
                                showAlert = true
                            }
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 12)
                                    .stroke(Color.silver, lineWidth: 2)
                                    .background(Color.white)
                                    .cornerRadius(12)
                                    .frame(width: 150, height: 150)
                                VStack {
                                    Image("main_img_03")
                                        .resizable()
                                        .frame(width: 142, height: 81)
                                    Text("상황실 교육")
                                        .font(.custom("Spoqa Han Sans Neo Regular", size: 18))
                                        .foregroundColor(Color.black)
                                        .padding(.top, 6)
                                }
                            }
                        }
                        .alert("알림", isPresented: $showAlert) {
                            Button("확인") {
                            }
                        } message: {
                            Text("금일 운행스케쥴이 없어 해당 메뉴를 실행할 수 없습니다.")
                        }
                    }
                    NavigationLink(destination: SuitabilityView(), tag: "suitability", selection: $destination) {
                        Button(action: {
                            if config.getIsWorkday() == true {
                                destination = "suitability"
                            }
                            else {
                                showAlert = true
                            }
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 12)
                                    .stroke(Color.silver, lineWidth: 2)
                                    .background(Color.white)
                                    .cornerRadius(12)
                                    .frame(width: 150, height: 150)
                                VStack {
                                    Image("main_img_04")
                                        .resizable()
                                        .frame(width: 142, height: 81)
                                    Text("승무적합성")
                                        .font(.custom("Spoqa Han Sans Neo Regular", size: 18))
                                        .foregroundColor(Color.black)
                                        .padding(.top, 6)
                                }
                            }
                            .padding(.leading, 10)
                        }
                        .alert("알림", isPresented: $showAlert) {
                            Button("확인") {
                            }
                        } message: {
                            Text("금일 운행스케쥴이 없어 해당 메뉴를 실행할 수 없습니다.")
                        }
                    }
                }
                HStack {
                    NavigationLink(destination: EndCrewView(), tag: "endCrew", selection: $destination) {
                        Button(action: {
                            if config.getIsWorkday() == true {
                                destination = "endCrew"
                            }
                            else {
                                showAlert = true
                            }
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 12)
                                    .stroke(Color.silver, lineWidth: 2)
                                    .background(Color.white)
                                    .cornerRadius(12)
                                    .frame(width: 150, height: 150)
                                VStack {
                                    Image("main_img_05")
                                        .resizable()
                                        .frame(width: 142, height: 81)
                                    Text("승무일지제출")
                                        .font(.custom("Spoqa Han Sans Neo Regular", size: 18))
                                        .foregroundColor(Color.black)
                                        .padding(.top, 6)
                                }
                            }
                        }
                    }
                    NavigationLink(destination: EmergencyView(), tag: "emergency", selection: $destination) {
                        Button(action: {
                            destination = "emergency"
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 12)
                                    .stroke(Color.silver, lineWidth: 2)
                                    .background(Color.white)
                                    .cornerRadius(12)
                                    .frame(width: 150, height: 150)
                                VStack {
                                    Image("main_img_06")
                                        .resizable()
                                        .frame(width: 142, height: 81)
                                    Text("비상연락·이례사항")
                                        .font(.custom("Spoqa Han Sans Neo Regular", size: 18))
                                        .foregroundColor(Color.black)
                                        .padding(.top, 6)
                                }
                            }
                            .padding(.leading, 10)
                        }
                    }
                }
                Spacer()
                Button(action: {
                    logoutActive = false
                }) {
                    ZStack {
                        RoundedRectangle(cornerRadius: 8)
                            .background(Color.brightBlue)
                            .frame(width: 312, height: 56)
                        HStack {
                            Image("main_ic_logout")
                                .resizable()
                                .frame(width: 27, height: 27)
                            Text("로그아웃")
                                .foregroundColor(Color.white)
                                .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                        }
                    }
                    .cornerRadius(8)
                }
            }
            .navigationBarBackButtonHidden(true)
            .onAppear() {
                Task {
                    let checkPreSche = await getPreSchedule()
                    if checkPreSche == true {
                        if config.getIsFirstLogin() == true {
                            await getApi.getProject()
                            config.setIsFirstLogin(firstLogin: false)
                        }
                    }
                    getDiaUrl()
                }
            }
            
            VStack {
                Color.black.opacity(0.5).ignoresSafeArea()
            }
            .isHidden(showCrewInfoMenu || showEduMenu ? false : true)
            .onTapGesture() {
                withAnimation {
                    showCrewInfoMenu = false
                    showEduMenu = false
                }
                
            }
            
            VStack {
                if showCrewInfoMenu {
                    CrewInfoMenuView()
                        .transition(.move(edge: .bottom))
                }
            }
            .animation(.easeInOut(duration: 0.5), value: showCrewInfoMenu)
            
            VStack {
                if showEduMenu {
                    EducationMenuView()
                        .transition(.move(edge: .bottom))
                }
            }
            .animation(.easeInOut(duration: 0.5), value: showEduMenu)
        }
    }
    
    
    
    // 승무기본정보 조회
    func getPreSchedule() async -> Bool {
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "getPreSchedule", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "date":util.getToday()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        
        let postData = "p="+jsonString!
        apiGetPreSchedule(strUrl: "getMtitData2.jsp", postData: postData, success: { [self] (results) in
            self.config.setIsWorkday(workday: true)
            if(results?.ALC_SPEC_DV_CD == "12") {
                self.config.setPjtDt(pjtDt: util.getYesterday())
            }
            else if(results?.ALC_SPEC_DV_CD == "11"){
                self.config.setPjtDt(pjtDt: results!.PJT_DT ?? "")
            }
            
            if(results?.GWK_TM == nil) {
                self.config.setIsWorkday(workday: false)
            }
            self.config.setBlgCd(blgCd: results!.BLG_CD ?? "")
            self.config.setBlgNm(blgNm: results!.BLG_NM ?? "")
            self.config.setDiaNo(diaNo: results!.PDIA_NO ?? "")
            self.config.setDiaDvCd(diaDvCd: results!.PDIA_DV_CD ?? "")
            self.config.setCrewPlanCd(crewPlanCd: results!.CRW_PLN_DV_CD ?? "")
            self.config.setCrewPlanNm(crewPlanNm: results!.CRW_PLN_NM ?? "")
            self.config.setUserName(userName: results!.EMP_NM ?? "")
            self.config.setGWK_tm(gwk: results!.GWK_TM ?? "")
            self.config.setLOIW_tm(loiw: results!.LOIW_TM ?? "")
            self.config.setTotTm(totTm: results!.TOT_WRK_TNUM ?? "")
            self.config.setTotDst(totDst: results!.TOT_PJT_DST ?? "")
            self.config.setDiaStartDate(diaDvCd: results!.PDIA_EFC_ST_DT ?? "")
            self.config.setWorkDays(workDays: results!.ND_DNO ?? "")
            semaphore.signal()
        }) { (err) in
            print("getPreSchedule error \(String(describing: err))")
            DispatchQueue.main.async {
                
            }
        }
        semaphore.wait()
        return true
    }
    
    func getDiaUrl() {
        let dictionary = ["order": "get_diagram", "crew_id":config.getUserId(), "access_key":config.getAccessKey(), "schedule_date":config.getPjtDt(), "blg_nm":config.getBlgNm()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
    
        let postData = "p="+jsonString!
        apiGetDiagram(strUrl: "getMtitData2.jsp", postData: postData, success: { (results) in
            config.setDiaUrl(diaUrl: results!.image_path)
 
        }) { (err) in
            print("apiDeviceSerialList error \(String(describing: err))")
        }
    }
}

struct CrewInfoMenuView: View {
    @GestureState private var translation: CGFloat = 0
    var offset: CGFloat = 0
    
    @State var destination: String? = ""
    
    var body: some View {
        GeometryReader{ geometry in
            VStack(alignment: .leading, spacing: 0) {
                Text("승무정보")
                    .font(.custom("Spoqa Han Sans Neo Bold", size: 20))
                    .foregroundColor(Color.black)
                HStack {
                    NavigationLink(destination: DiaView(), tag: "dia", selection: $destination) {
                        Button(action: {
                            destination = "dia"
                        }) {
                            ZStack {
                                Rectangle()
                                    .fill(Color.brightBlue)
                                    .cornerRadius(8)
                                    .frame(width: 152, height: 56)
                                Text("사업다이아")
                                    .foregroundColor(Color.white)
                                    .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                            }
                            
                        }
                    }
                    NavigationLink(destination: EfcDeliverView(), tag: "efcDeliver", selection: $destination) {
                        Button(action: {
                            destination = "efcDeliver"
                        }) {
                            ZStack {
                                Rectangle()
                                    .fill(Color.brightBlue)
                                    .cornerRadius(8)
                                    .frame(width: 152, height: 56)
                                Text("시행전달부")
                                    .foregroundColor(Color.white)
                                    .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                            }
                        }
                    }
                    
                }
                .padding(.top, 23)
                
                HStack {
                    NavigationLink(destination: InstructionView(), tag: "instruction", selection: $destination) {
                        Button(action: {
                            destination = "instruction"
                        }) {
                            ZStack {
                                Rectangle()
                                    .fill(Color.brightBlue)
                                    .cornerRadius(8)
                                    .frame(width: 152, height: 56)
                                Text("지시 및 전달사항")
                                    .foregroundColor(Color.white)
                                    .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                            }
                            
                        }
                    }
                    
                    Button(action: {
                        
                    }) {
                        ZStack {
                            Rectangle()
                                .fill(Color.brightBlue)
                                .cornerRadius(8)
                                .frame(width: 152, height: 56)
                            Text("운전차량정보")
                                .foregroundColor(Color.white)
                                .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                        }
                    }
                }
                .padding(.top, 7)
                
                HStack {
                    Button(action: {
                        
                    }) {
                        ZStack {
                            Rectangle()
                                .fill(Color.brightBlue)
                                .cornerRadius(8)
                                .frame(width: 152, height: 56)
                            Text("승무원 면허정보")
                                .foregroundColor(Color.white)
                                .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                        }
                        
                    }
                    
                    Button(action: {
                        
                    }) {
                        ZStack {
                            Rectangle()
                                .fill(Color.brightBlue)
                                .cornerRadius(8)
                                .frame(width: 152, height: 56)
                            Text("휴대품 확인")
                                .foregroundColor(Color.white)
                                .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                        }
                    }
                }
                .padding(.top, 7)
            }
            .padding(.top, 25)
            .padding(.leading, 23)
            .padding(.trailing, 23)
            .frame(width: geometry.size.width, height: 262, alignment: .top)
            .background(
                Rectangle()
                    .fill(.white)
                    .cornerRadius(20, corners: [.topLeft, .topRight])
                    .edgesIgnoringSafeArea([.bottom, .horizontal])
                    .shadow(radius: 20)
            )
            .frame(height: geometry.size.height, alignment: .bottom)
            .transition(.move(edge: .bottom))
            
        }
    }
}

struct EducationMenuView: View {
    @GestureState private var translation: CGFloat = 0
    var offset: CGFloat = 0
    
    var body: some View {
        GeometryReader{ geometry in
            VStack(alignment: .leading, spacing: 0) {
                Text("상황실 교육")
                    .font(.custom("Spoqa Han Sans Neo Bold", size: 20))
                    .foregroundColor(Color.black)
                HStack {
                    Button(action: {
                        
                    }) {
                        ZStack {
                            Rectangle()
                                .fill(Color.brightBlue)
                                .cornerRadius(8)
                                .frame(width: 152, height: 56)
                            Text("교육자료")
                                .foregroundColor(Color.white)
                                .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                        }
                        
                    }
                    
                    Button(action: {
                        
                    }) {
                        ZStack {
                            Rectangle()
                                .fill(Color.brightBlue)
                                .cornerRadius(8)
                                .frame(width: 152, height: 56)
                            Text("도우미 요청")
                                .foregroundColor(Color.white)
                                .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                        }
                    }
                }
                .padding(.top, 23)
            }
            .padding(.top, 25)
            .padding(.leading, 23)
            .padding(.trailing, 23)
            .frame(width: geometry.size.width, height: 137, alignment: .top)
            .background(
                Rectangle()
                    .fill(.white)
                    .cornerRadius(20, corners: [.topLeft, .topRight])
                    .edgesIgnoringSafeArea([.bottom, .horizontal])
                    .shadow(radius: 20)
            )
            .frame(height: geometry.size.height, alignment: .bottom)
            .transition(.move(edge: .bottom))
        }
    }
}





extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
}

struct RoundedCorner: Shape {

    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

extension View {
    @ViewBuilder func isHidden(_ hidden: Bool, remove: Bool = false) -> some View {
        if hidden {
            if !remove {
                self.hidden()
            }
        } else {
            self
        }
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView(logoutActive: .constant(true))
    }
}

/*struct LocationAgreeView: View {
    let locationManager = LocationManager()
    
    @GestureState private var translation: CGFloat = 0
    var offset: CGFloat = 0
    
    @Binding var showLocationAgree: Bool
    
    var body: some View {
        GeometryReader{ geometry in
            VStack(alignment: .leading, spacing: 0) {
                Text("승무 전 확인사항")
                    .font(.custom("Spoqa Han Sans Neo Bold", size: 20))
                    .foregroundColor(Color.black)
                Text("승무사업의 출무를 위해 귀하의 위치정보를 수\n신합니다.")
                    .font(.custom("Spoqa Han Sans Neo Regular", size: 16))
                    .foregroundColor(Color.black)
                    .padding(.top, 12)
                HStack {
                    Button(action: {
                        
                    }) {
                        ZStack {
                            Rectangle()
                                .fill(Color.brownish_grey)
                                .cornerRadius(8)
                                .frame(width: 152, height: 56)
                            Text("동의하지 않음")
                                .foregroundColor(Color.white)
                                .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                        }
                        
                    }
                    
                    Button(action: {
                        locationManager.requestPermission()
                        showLocationAgree = false
                    }) {
                        ZStack {
                            Rectangle()
                                .fill(Color.brightBlue)
                                .cornerRadius(8)
                                .frame(width: 152, height: 56)
                            Text("동의")
                                .foregroundColor(Color.white)
                                .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                        }
                    }
                }
                .padding(.top, 23)
            }
            .padding(.top, 25)
            .padding(.leading, 23)
            .padding(.trailing, 23)
            .frame(width: geometry.size.width, height: 210, alignment: .top)
            .background(
                Rectangle()
                    .fill(.white)
                    .cornerRadius(20, corners: [.topLeft, .topRight])
                    .edgesIgnoringSafeArea([.bottom, .horizontal])
                    .shadow(radius: 20)
            )
            .frame(height: geometry.size.height, alignment: .bottom)
            .offset(y: max(self.offset + self.translation, 0))
            .animation(.interactiveSpring(), value: offset)
        }
    }
}*/
