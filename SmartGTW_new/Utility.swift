//
//  Utility.swift
//  MtitCrew
//
//  Created by 코레일 on 2021/04/22.
//

import Foundation

class Utility {
    func getToday() -> String {
        let now=NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //currDate.text = dateFormatter.stringFromDate(now)
        return dateFormatter.string(from: now as Date)
    }
    
    func getNowTime() -> String {
        let now=NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //currDate.text = dateFormatter.stringFromDate(now)
        return dateFormatter.string(from: now as Date)
    }
    
    func getYesterday() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: date)
        
        return dateFormatter.string(from: yesterday! as Date)
    }
    
    func convertFormattedDate(date: String, containYear: Bool, containTime: Bool) -> String {
        var result = date
        if date.count >= 14 {
            let index1 = date.index(date.startIndex, offsetBy: 4)
            let year = date[..<index1]
            
            let index2 = date.index(date.endIndex, offsetBy: -10)
            let timeTemp1 = date[index2...]
            
            let index3 = timeTemp1.index(timeTemp1.startIndex, offsetBy: 2)
            let month = timeTemp1[..<index3]

            let index4 = date.index(date.endIndex, offsetBy: -8)
            let timeTemp2 = date[index4...]
            
            let index5 = timeTemp2.index(timeTemp2.startIndex, offsetBy: 2)
            let day = timeTemp2[..<index5]

            let index6 = date.index(date.endIndex, offsetBy: -6)
            let timeTemp3 = date[index6...]

            let index7 = timeTemp3.index(timeTemp3.startIndex, offsetBy: 2)
            let hour = timeTemp3[..<index7]

            let index8 = date.index(date.endIndex, offsetBy: -4)
            let timeTemp4 = date[index8...]

            let index9 = timeTemp4.index(timeTemp4.startIndex, offsetBy: 2)
            let minute = timeTemp4[..<index9]

            let index10 = date.index(date.endIndex, offsetBy: -2)
            let sec = date[index10...]

            if containYear == true {
                if containTime == true {
                    result = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + sec
                } else {
                    result = year + "-" + month + "-" + day
                }
            } else {
                if containTime == true {
                    //result = month + "-" + day + " " + hour + ":" + minute + ":" + sec
                    result = hour + ":" + minute
                } else {
                    result = month + "-" + day
                }
            }
        }
        
        return result
    }
    
    func convertFormattedTime(time: String) -> String {
        var result = time
        if time.count >= 4 {
            let index1 = time.index(time.startIndex, offsetBy: 2)
            let hour = time[..<index1]
            
            let index2 = time.index(time.endIndex, offsetBy: -2)
            let minute = time[index2...]

            result = hour + ":" + minute
        }
        
        return result
    }
    
    func getReturnDate(date: String) -> String {
        let yearIdx: String.Index = date.index(date.startIndex, offsetBy: 3)
        let year = String(date[...yearIdx])
        
        let monIdx: String.Index = date.index(date.startIndex, offsetBy: 4)
        let monIdx2: String.Index = date.index(date.startIndex,offsetBy:5)
        let month = String(date[monIdx...monIdx2])
        
        let dayIdx: String.Index = date.index(date.startIndex, offsetBy: 6)
        let day = String(date[dayIdx...])
        
        return year+"-"+month+"-"+day+" ("+getWeekDay(atYear: Int(year)!, atMonth: Int(month)!, atDay: Int(day)!)+")"
    }
    
    func endOfMonth(atMonth: Int) -> Int {
        let set30: [Int] = [1,3,5,7,8,10,12]
        var endDay: Int = 0
        if atMonth == 2 {
            endDay = 28
        }else if set30.contains(atMonth) {
            endDay = 31
        }else {
            endDay = 30
        }
        
        return endDay
    }
    
    func getWeekDay(atYear:Int, atMonth:Int, atDay:Int) -> String {
        
        let dayDay:[String] = ["토", "일", "월", "화", "수", "목", "금"]
        var returnValue: String = ""
        var totalDay: Int = 0
        
        for i in 1..<atMonth {
            totalDay += endOfMonth(atMonth: i)
        }
        
        var index: Int = 0
        if (totalDay + atDay) % 7 == 0 {
            index = 6
        }else {
            index = (totalDay + atDay) % 7 - 1
        }
        
        returnValue = dayDay[index]
        
        return returnValue
    }
    
    func getHexColor(color: String, alpah: CGFloat) -> UIColor {
        let scanner = Scanner(string: color)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r: CGFloat = CGFloat((rgbValue & 0xff0000) >> 16)
        let g: CGFloat = CGFloat((rgbValue & 0xff00) >> 8)
        let b: CGFloat = CGFloat(rgbValue & 0xff)
        
        let R: CGFloat = r / 255.0
        let G: CGFloat = g / 255.0
        let B: CGFloat = b / 255.0
        
        //print("konnect rgb: \(r), \(g), \(b)")
        //print("konnect RGB: \(R), \(G), \(B)")

        return UIColor(red: R, green: G, blue: B, alpha: alpah)
    }
    
    func getLoginDate() -> String {
        let cal = Calendar.current
        let date = Date()
        let year = cal.component(.year, from: date)
        let month = cal.component(.month, from: date)
        let day = cal.component(.day, from: date)
        
        return getToday()+" ("+getWeekDay(atYear: year, atMonth: month, atDay: day)+")"
    }
    
    // 출퇴근시간 계산
    func getWorkTime(time: String) -> String {        
        let idx: String.Index = time.index((time.startIndex), offsetBy: 1, limitedBy: time.endIndex)!
        let idx2: String.Index = time.index((time.startIndex), offsetBy: 2, limitedBy: time.endIndex)!
        let workHour = String(time[...idx])
        let workMin = String(time[idx2...])
        return workHour+":"+workMin
    }
    
    // 총 사업시간 계산
    func getTotTime(time: String) -> String {
        let totTime = Int(time)!
        let timeHour = totTime / 60
        let timeMin = totTime % 60
        
        return String(timeHour)+":"+String(format:"%02d", timeMin)
    }
    
    // 사업거리 계산
    func getDst(dst: String) -> String {
        let dstIdx: String.Index = dst.index((dst.startIndex), offsetBy: 4, limitedBy: dst.endIndex)!
        let dstResult = String(dst[...dstIdx])
        
        return dstResult
    }
    
    func getPjtTime(time: String) -> String {
        let idx: String.Index = time.index((time.startIndex), offsetBy: 1, limitedBy: time.endIndex)!
        let idx2: String.Index = time.index((time.startIndex), offsetBy: 2, limitedBy: time.endIndex)!
        let idx3: String.Index = time.index((time.startIndex), offsetBy: 3, limitedBy: time.endIndex)!
        let workHour = String(time[...idx])
        let workMin = String(time[idx2...idx3])
        return workHour+":"+workMin
    }
    
    func getFormattedTime(time: String, bk: Bool) -> String {
        var index = time.index(time.startIndex, offsetBy: 2)
        let hour = time.prefix(upTo: index)
        
        index = time.index(time.endIndex, offsetBy: -2)
        let second = time.suffix(from: index)

        index = time.index(time.startIndex, offsetBy: 4)
        let minTemp = time.prefix(upTo: index)

        index = minTemp.index(minTemp.endIndex, offsetBy: -2)
        let minute = minTemp.suffix(from: index)

        let formattedTime = hour + ":" + minute + ":" + second
        
        if (hour == "  ") {
            return ""
        } else {
            if bk == true {
                return "(" + formattedTime + ")"
            } else {
                return formattedTime
            }
        }
    }
    
    func getMonthDay(date: String) -> String {
        let idx: String.Index = date.index((date.startIndex), offsetBy: 4, limitedBy: date.endIndex)!
        let idx2: String.Index = date.index((date.startIndex), offsetBy: 5, limitedBy: date.endIndex)!
        let idx3: String.Index = date.index((date.startIndex), offsetBy: 6, limitedBy: date.endIndex)!
        let month = String(date[idx...idx2])
        let day = String(date[idx3...])
        return month+"-"+day
    }
}
