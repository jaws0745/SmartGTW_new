//
//  LocationManager.swift
//  SmartGTW_new
//
//  Created by 코레일 on 2022/08/29.
//

import Foundation
import CoreLocation
import SwiftUI

class LocationManager : NSObject, CLLocationManagerDelegate {
    @Published var authorizationStatus: CLAuthorizationStatus
    private let locationManager: CLLocationManager
    
    @Published var latitude: Double?
    @Published var longitude: Double?
    
    override init() {
        locationManager = CLLocationManager()
        authorizationStatus = locationManager.authorizationStatus
        
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        let coor = locationManager.location?.coordinate
        latitude = coor?.latitude
        longitude = coor?.longitude
        
    }
    
    func requestPermission() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        authorizationStatus = manager.authorizationStatus
    }
    
    func updateLocation() {
        locationManager.startUpdatingLocation()
        let coor = locationManager.location?.coordinate
        latitude = coor?.latitude
        longitude = coor?.longitude
    }
}
