//
//  DBHelper.swift
//  MtitCrew
//
//  Created by 코레일 on 2022/06/16.
//

import Foundation
import SQLite3

class DBHelper {
    static let shared = DBHelper()
    
    var db : OpaquePointer?
    
    let databaseName = "contactDB.sqlite"
    
    init() {
        self.db = createDB()
    }
    
    deinit {
        sqlite3_close(db)
    }
    
    private func createDB() -> OpaquePointer? {
        var db: OpaquePointer? = nil
        do {
            let dbPath: String = try FileManager.default.url(
                for: .documentDirectory,
                in: .userDomainMask,
                appropriateFor: nil,
                create: false).appendingPathComponent(databaseName).path
            
            if sqlite3_open(dbPath, &db) == SQLITE_OK {
                print("Successfully created DB. Path: \(dbPath)")
                return db
            }
        } catch {
            print("Error while creating Database -\(error.localizedDescription)")
        }
        return nil
    }
    
    func createTable(){
        // 아래 query의 뜻.
        // mytable이라는 table을 생성한다. 필드는
        // id(int, auto-increment primary key)
        // my_name(String not null)
        // my_age(Int)
        // 로 구성한다.
        // auto-increment 속성은 INTEGER에만 가능하다.
        let query = """
               CREATE TABLE IF NOT EXISTS myTable(
               OID INTEGER PRIMARY KEY AUTOINCREMENT,
               DEPT_NAME TEXT,
               POSITION TEXT,
               PHONE_NO TEXT
               );
               """
        var statement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(self.db, query, -1, &statement, nil) == SQLITE_OK {
            if sqlite3_step(statement) == SQLITE_DONE {
                print("Creating table has been succesfully done. db: \(String(describing: self.db))")
                
            }
            else {
                let errorMessage = String(cString: sqlite3_errmsg(db))
                print("\nsqlte3_step failure while creating table: \(errorMessage)")
            }
        }
        else {
            let errorMessage = String(cString: sqlite3_errmsg(self.db))
            print("\nsqlite3_prepare failure while creating table: \(errorMessage)")
        }
        
        sqlite3_finalize(statement) // 메모리에서 sqlite3 할당 해제.
    }
    
    func insertData(DEPT_NAME: String, POSITION: String, PHONE_NO: String) {
        // id 는 Auto increment 속성을 갖고 있기에 빼줌.
        let insertQuery = "insert into myTable (OID, DEPT_NAME, POSITION, PHONE_NO) values (?, ?, ?, ?);"
        //print(insertQuery)
        var statement: OpaquePointer? = nil
        
        if sqlite3_prepare_v2(self.db, insertQuery, -1, &statement, nil) == SQLITE_OK {
            sqlite3_bind_text(statement, 2, NSString(string: DEPT_NAME).utf8String, -1, nil)
            sqlite3_bind_text(statement, 3, NSString(string: POSITION).utf8String, -1, nil)
            sqlite3_bind_text(statement, 4, NSString(string: PHONE_NO).utf8String, -1, nil)
            print(insertQuery)
            
        }
        else {
            print("sqlite binding failure")
        }
        
        if sqlite3_step(statement) == SQLITE_DONE {
            print("sqlite insertion success")
        }
        else {
            print("sqlite step failure")
        }
    }
    
    func readData() -> [CONTACT_MODEL] {
        let query: String = "select * from myTable;"
        var statement: OpaquePointer? = nil
        // 아래는 [MyModel]? 이 되면 값이 안 들어간다.
        // Nil을 인식하지 못하는 것으로..
        var result: [CONTACT_MODEL] = []
        
        if sqlite3_prepare(self.db, query, -1, &statement, nil) != SQLITE_OK {
            let errorMessage = String(cString: sqlite3_errmsg(db)!)
            print("error while prepare: \(errorMessage)")
            return result
        }
        while sqlite3_step(statement) == SQLITE_ROW {
            
            let id = sqlite3_column_int(statement, 0) // 결과의 0번째 테이블 값
            let dept_name = String(cString: sqlite3_column_text(statement, 1)) // 결과의 1번째 테이블 값.
            let position = String(cString: sqlite3_column_text(statement, 2))
            let phone_no = String(cString: sqlite3_column_text(statement, 3))
            //let checked = String(cString: sqlite3_column_text(statement, 4))
            
            result.append(CONTACT_MODEL(id: Int(id), dept_name: String(dept_name), position: String(position), phone_no: String(phone_no), checked: false))
        }
        sqlite3_finalize(statement)
        
        return result
    }
    
    func deleteTable(tableName: String) {
        let queryString = "DROP TABLE \(tableName)"
        var statement: OpaquePointer?
        
        if sqlite3_prepare(db, queryString, -1, &statement, nil) != SQLITE_OK {
            //onSQLErrorPrintErrorMessage(db)
            return
        }
        
        // 쿼리 실행.
        if sqlite3_step(statement) != SQLITE_DONE {
            //onSQLErrorPrintErrorMessage(db)
            return
        }
        
        print("drop table has been successfully done")
        
    }
    
}
