//
//  SqliteHandler.swift
//  smartoffice
//
//  Created by kim on 19/11/2018.
//  Copyright © 2018 korail. All rights reserved.
//

import Foundation
import UIKit
import SQLite3

class SqliteHandler {
    
    var databasePath = NSString() //SQLite 데이터베이스 파일 경로
    var contactDB: FMDatabase! = nil //SQLite 관리자
    
    func initDataBase() {
        // 애플리케이션이 실행되면 데이터베이스 파일이 존재하는지 체크한다. 존재하지 않으면 데이터베이스파일과 테이블을 생성한다.
        let filemgr = FileManager.default
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docsDir = dirPaths[0] as String
        databasePath = docsDir.appendingFormat("/talk.db")
        contactDB = FMDatabase(path:databasePath as String)
        
        if !filemgr.fileExists(atPath: databasePath as String) {
            
            // FMDB 인스턴스를 이용하여 DB 체크
            if contactDB == nil {
                print("[1] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }
            
            // DB 오픈
            if (contactDB?.open())!{
                // 테이블 생성처리
                var sql_stmt = "PRAGMA auto_vacuum = 1"
                if !(contactDB?.executeStatements(sql_stmt))! {
                    print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
                }
                sql_stmt = "CREATE TABLE IF NOT EXISTS t_contact (OID INTEGER PRIMARY KEY autoincrement, DEPT_NAME TEXT, POSITION TEXT, PHONE_NO TEXT)"
                if !(contactDB?.executeStatements(sql_stmt))! {
                    print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
                }
                
//                sql_stmt = "CREATE TABLE IF NOT EXISTS t_message_received (OID INTEGER PRIMARY KEY autoincrement, TYPE TEXT, PID INTEGER, ROOM_ID TEXT, ID_FROM TEXT, RECEIVED_DATE TEXT, READ_YN VARCHAR(1) DEFAULT 'N', NOTIFY_ID INTEGER)"
//                if !(contactDB?.executeStatements(sql_stmt))! {
//                    print("[2] Error : \(contactDB?.lastErrorMessage())")
//                }
                
//                sql_stmt = "DROP TABLE IF EXISTS t_message_log"
//                if !(contactDB?.executeStatements(sql_stmt))! {
//                    print("[2] Error : \(contactDB?.lastErrorMessage())")
//                }
                
                sql_stmt = "CREATE TABLE IF NOT EXISTS t_message_log (OID INTEGER PRIMARY KEY autoincrement, PID INTEGER, TYPE TEXT, RS_TYPE TEXT, ROOM_ID TEXT, ID_FROM TEXT, PHOTO TEXT, NAME_FROM TEXT, DEPT_FROM TEXT, MSG_TYPE TEXT, MESSAGE TEXT, REG_DATE TEXT)"
                if !(contactDB?.executeStatements(sql_stmt))! {
                    print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
                }
                
//                sql_stmt = "DROP TABLE IF EXISTS t_talk_list"
//                if !(contactDB?.executeStatements(sql_stmt))! {
//                    print("[2] Error : \(contactDB?.lastErrorMessage())")
//                }

                sql_stmt = "CREATE TABLE IF NOT EXISTS t_talk_list (OID INTEGER PRIMARY KEY autoincrement, TYPE TEXT, ROOM_ID TEXT, ROOM_NAME TEXT, DEPT_NAME TEXT, REG_DATE TEXT, PHOTO TEXT, UPDATE_DATE TEXT, NOT_READ INTEGER DEFAULT 0)"
                if !(contactDB?.executeStatements(sql_stmt))! {
                    print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
                }
                
                sql_stmt = "CREATE TABLE IF NOT EXISTS t_topic_subscribe (OID INTEGER PRIMARY KEY autoincrement, TYPE TEXT, PID INTEGER, REG_DATE TEXT)"
                if !(contactDB?.executeStatements(sql_stmt))! {
                    print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
                }
                
                sql_stmt = "CREATE TABLE IF NOT EXISTS t_title_image (OID INTEGER PRIMARY KEY autoincrement, IMAGE TEXT, REG_DATE TEXT)"
                if !(contactDB?.executeStatements(sql_stmt))! {
                    print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
                }
                
                sql_stmt = "CREATE TABLE IF NOT EXISTS t_subscribe (OID INTEGER PRIMARY KEY autoincrement, TOPIC TEXT, REG_DATE TEXT)"
                if !(contactDB?.executeStatements(sql_stmt))! {
                    print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
                }
                
                sql_stmt = "CREATE TABLE IF NOT EXISTS t_setting_alarm (OID INTEGER PRIMARY KEY autoincrement, ROOM_ID TEXT, ALARM TEXT)"
                if !(contactDB?.executeStatements(sql_stmt))! {
                    print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
                }
                
                sql_stmt = "CREATE TABLE IF NOT EXISTS t_read_news (OID INTEGER PRIMARY KEY autoincrement, PID INTEGER)"
                if !(contactDB?.executeStatements(sql_stmt))! {
                    print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
                }

                sql_stmt = "CREATE TABLE IF NOT EXISTS t_news_crawling_grouping (VID INTEGER, OID INTEGER, GROUP_ID INTEGER, SEQ INTEGER, TITLE TEXT, CONTENT TEXT, USER_NAME TEXT, DEPT_NAME TEXT, REG_DATE TEXT)"
                if !(contactDB?.executeStatements(sql_stmt))! {
                    print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
                }

                contactDB?.close()
            }else{
                print("[3] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }
        }else{
            print("[1] SQLite 파일 존재!!")
        }
        
        //테이블 생성
        if (contactDB?.open())!{
            // 테이블 생성처리
            var sql_stmt = "PRAGMA auto_vacuum = 1"
            if !(contactDB?.executeStatements(sql_stmt))! {
                print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }

//            sql_stmt = "DROP TABLE IF EXISTS t_news_crawling_grouping"
//            if !(contactDB?.executeStatements(sql_stmt))! {
//                print("[2] Error : \(contactDB?.lastErrorMessage())")
//            }

//            sql_stmt = "DROP TABLE IF EXISTS t_message_received"
//            if !(contactDB?.executeStatements(sql_stmt))! {
//                print("[2] Error : \(contactDB?.lastErrorMessage())")
//            }
//
//            sql_stmt = "DROP TABLE IF EXISTS t_message_log"
//            if !(contactDB?.executeStatements(sql_stmt))! {
//                print("[2] Error : \(contactDB?.lastErrorMessage())")
//            }

//            sql_stmt = "DROP TABLE IF EXISTS t_contact"
//            if !(contactDB?.executeStatements(sql_stmt))! {
//                print("[2] Error : \(contactDB?.lastErrorMessage())")
//            }

            sql_stmt = "CREATE TABLE IF NOT EXISTS t_contact (OID INTEGER PRIMARY KEY autoincrement, DEPT_NAME TEXT, POSITION TEXT, PHONE_NO TEXT)"
            if !(contactDB?.executeStatements(sql_stmt))! {
                print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }
            
//            sql_stmt = "CREATE TABLE IF NOT EXISTS t_message_received (OID INTEGER PRIMARY KEY autoincrement, TYPE TEXT, PID INTEGER, ROOM_ID TEXT, ID_FROM TEXT, RECEIVED_DATE TEXT, READ_YN VARCHAR(1) DEFAULT 'N', NOTIFY_ID INTEGER)"
//            if !(contactDB?.executeStatements(sql_stmt))! {
//                print("[2] Error : \(contactDB?.lastErrorMessage())")
//            }
//            sql_stmt = "DROP TABLE IF EXISTS t_message_log"
//            if !(contactDB?.executeStatements(sql_stmt))! {
//                print("[2] Error : \(contactDB?.lastErrorMessage())")
//            }

            sql_stmt = "CREATE TABLE IF NOT EXISTS t_message_log (OID INTEGER PRIMARY KEY autoincrement, PID INTEGER, TYPE TEXT, RS_TYPE TEXT, ROOM_ID TEXT, ID_FROM TEXT, PHOTO TEXT, NAME_FROM TEXT, DEPT_FROM TEXT, MSG_TYPE TEXT, MESSAGE TEXT, REG_DATE TEXT)"
            if !(contactDB?.executeStatements(sql_stmt))! {
                print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }
            
//            sql_stmt = "DROP TABLE IF EXISTS t_talk_list"
//            if !(contactDB?.executeStatements(sql_stmt))! {
//                print("[2] Error : \(contactDB?.lastErrorMessage())")
//            }

            sql_stmt = "CREATE TABLE IF NOT EXISTS t_talk_list (OID INTEGER PRIMARY KEY autoincrement, TYPE TEXT, ROOM_ID TEXT, ROOM_NAME TEXT, DEPT_NAME TEXT, REG_DATE TEXT, PHOTO TEXT, UPDATE_DATE TEXT, NOT_READ INTEGER DEFAULT 0)"
            if !(contactDB?.executeStatements(sql_stmt))! {
                print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS t_image (OID INTEGER PRIMARY KEY autoincrement, TYPE TEXT, PID INTEGER, IMG_NAME TEXT)"
            if !(contactDB?.executeStatements(sql_stmt))! {
                print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS t_topic_subscribe (OID INTEGER PRIMARY KEY autoincrement, TYPE TEXT, PID INTEGER, REG_DATE TEXT)"
            if !(contactDB?.executeStatements(sql_stmt))! {
                print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }

            sql_stmt = "CREATE TABLE IF NOT EXISTS t_title_image (OID INTEGER PRIMARY KEY autoincrement, IMAGE TEXT, REG_DATE TEXT)"
            if !(contactDB?.executeStatements(sql_stmt))! {
                print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }

            sql_stmt = "CREATE TABLE IF NOT EXISTS t_subscribe (OID INTEGER PRIMARY KEY autoincrement, TOPIC TEXT, REG_DATE TEXT)"
            if !(contactDB?.executeStatements(sql_stmt))! {
                print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS t_setting_alarm (OID INTEGER PRIMARY KEY autoincrement, ROOM_ID TEXT, ALARM TEXT)"
            if !(contactDB?.executeStatements(sql_stmt))! {
                print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }

            sql_stmt = "CREATE TABLE IF NOT EXISTS t_read_news (OID INTEGER PRIMARY KEY autoincrement, PID INTEGER)"
            if !(contactDB?.executeStatements(sql_stmt))! {
                print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS t_news_crawling_grouping (VID INTEGER, OID INTEGER, GROUP_ID INTEGER, SEQ INTEGER, TITLE TEXT, CONTENT TEXT, USER_NAME TEXT, DEPT_NAME TEXT, REG_DATE TEXT)"
            if !(contactDB?.executeStatements(sql_stmt))! {
                print("[2] Error : \(contactDB?.lastErrorMessage() ?? "")")
            }

            contactDB?.close()
        }else{
            print("[3] Error : \(contactDB?.lastErrorMessage() ?? "")")
        }
        
        /*
         Name textfield로부터 입력받은 이름 문자열 기준으로 DB로부터 해당 데이터 존재하는지 탐색
         */
        //let contactDB = FMDatabase(path: databasePath as String)
    }
    
    func connectSqliteDatabase() -> FMDatabase {
        var databasePath = NSString() //SQLite 데이터베이스 파일 경로
        var sqliteDB: FMDatabase! = nil //SQLite 관리자
        
        let filemgr = FileManager.default
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let docsDir = dirPaths[0] as String
        databasePath = docsDir.appendingFormat("/talk.db")
        sqliteDB = FMDatabase(path:databasePath as String)
        
        if !filemgr.fileExists(atPath: databasePath as String) {
            print("not found sqlite db file")
        }

        return sqliteDB
    }

    func closeSqliteDatabase(db: FMDatabase) {
        db.close()
    }
    
    func createSqliteDB() -> OpaquePointer {
        var db: OpaquePointer?
        let path: String = {
            let fm = FileManager.default
            return fm.urls(for:.libraryDirectory, in:.userDomainMask).last!
                .appendingPathComponent("/talk.db").path
        }()
        
        if sqlite3_open(path, &db) == SQLITE_OK {
        }
        
        return db!
    }
    
    func closeSqliteDB(db: OpaquePointer) {
        sqlite3_close(db)
        
    }
}
