//
//  GetDataClass.swift
//  CrewDiary
//
//  Created by 코레일 on 2022/07/29.
//

import Foundation

class RequestAPI: ObservableObject {
    static let shared = RequestAPI()
    let config = Config()
    let util = Utility()
    
    @Published var arrayOfPjt = [PROJECT]()
    @Published var arrayOfTrainDates = [String]()
    @Published var arrayOfTrains = [String]()
    @Published var arrayOfTimeTable = [TRAIN_SCHEDULE_ITEM]()
    @Published var arrayOfNoticeCode = [String]()
    @Published var arrayOfNotice = [NOTICE_ITEM]()
    @Published var arrayOfInstruction = [INSTRUCTION]()
    @Published var arrayOfResult = [WEB_RESULT]()
    @Published var result = ""
    @Published var descript = ""
    @Published var arrayOfSupplyCode = [CODE_ITEM]()
    @Published var arrayOfSupply = [SUPPLY_REGIST]()
    @Published var arrayOfDrivers = [DRIVER]()
    @Published var arrayOfDriver = [String]()
    @Published var arrayOfCheckRadio = [CHECK_RADIO]()
    @Published var arrayOfCarNo = [CARS]()
    @Published var arrayOfCarModel  = [CAR_MODEL]()
    @Published var arrayOfEquipCheckList = [CODE_ITEM]()
    @Published var arrayOfEquipFixList = [CODE_ITEM]()
    @Published var arrayOfCheckEquipment = [CHECK_EQUIPMENT]()
    @Published var arrayOfCarCheck = [CHECK_CAR]()
    @Published var arrayOfFaultMainCode = [CODE_ITEM]()
    @Published var arrayOfFaultMidCode = [CODE_ITEM]()
    @Published var arrayOfCarFault = [CAR_FAULT]()
    @Published var arrayOfTrainStation = [TRAIN_STATION]()
    @Published var arrayOfBroadMainCode = [CODE_ITEM]()
    @Published var arrayOfBroadMidCode = [CODE_ITEM]()
    @Published var arrayOfBroadSubCode = [CODE_ITEM]()
    @Published var broadMsg = ""
    @Published var arrayOfBroadCast = [BROADCAST]()
    @Published var arrayOfNextDia = [DIA_INFO]()
    @Published var arrayOfNextPjt = [PROJECT]()
    @Published var arrayOfContact = [CONTACTS]()
    @Published var arrayOfContactModel = [CONTACT_MODEL]()
    @Published var loading = false
    
    func getProject() async {
        arrayOfTrains.removeAll()
        
        let dictionary = ["order": "getSchedule", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(),
                          "crew_id":self.config.getUserId(),"dept_code":self.config.getBlgCd(),"schedule_date":self.config.getPjtDt(),"crew_plan_code":self.config.getCrewPlanCd()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            guard let response_str = String(data: data, encoding: .utf8) else { return }
            //print("apiGetProject response_str -> \(response_str)")
            do {
                let decode = try JSONDecoder().decode(PROJECT_LIST.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfPjt = decode.list
                    
                    for list in self.arrayOfPjt {
                        if (list.PJT_HR_DV_NM == "승무" || list.PJT_HR_DV_NM == "운전") {
                            let trnNo = String(list.TRN_NO ?? "")
                            let clfs = String(list.TRN_CLSF_NM ?? "")
                            self.arrayOfTrainDates.append(list.TRN_RUN_DT ?? "")
                            self.arrayOfTrains.append(clfs + " > " + trnNo)
                            let train = String(arrayOfTrains[0].split(separator: ">")[1])
                            self.config.setTrainText(trainText: self.arrayOfTrains[0])
                            self.config.setTrainDate(trainDate: self.arrayOfTrainDates[0])
                            self.config.setSelectedTrainNo(selectedTrainNo: train.trimmingCharacters(in: .whitespaces))
                        }
                    }
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func getSchedule() {
        let dictionary = ["order": "timeTableSimple", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":self.config.getTrainDate(), "train":self.config.getSelectedTrainNo()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(TRAIN_SCHEDULE.self, from: data)
                DispatchQueue.main.async {
                    self.arrayOfTimeTable = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func getNoticeCode() {
        arrayOfNoticeCode.append("전체보기")
        let dictionary = ["order": "base_code", "code":"TC0007", "access_key":self.config.getAccessKey()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CODE.self, from: data)
                DispatchQueue.main.async { [self] in
                    let lists = decode.list
                    for list in lists {
                        arrayOfNoticeCode.append(list.code_name)
                    }
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func getNotice() {
        let dictionary = ["order": "noticeA", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":self.config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "start_time":self.util.getToday()+" "+"00:00", "end_time":self.util.getToday()+" 24:00"]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(NOTICE.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfNotice = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func getInst() async {
        let semaphore = DispatchSemaphore(value: 0)
        loading = true
        let dictionary = ["order": "instruction", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp")
                
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(INSTRUCTION_LIST.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfInstruction = decode.list
                }
                semaphore.signal()
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
        semaphore.wait()
        loading = false
    }
    
    func setInst() async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "set_read_notice", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "work_days":self.config.getWorkDays(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func getSupplyCode() {
        let dictionary = ["order": "base_code", "code":"CR0126", "access_key":self.config.getAccessKey()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CODE.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfSupplyCode = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func getSupply() {
        arrayOfSupply.removeAll()
        
        let dictionary = ["order": "get_equipment", "access_key":self.config.getAccessKey(), "user_id":self.config.getUserId(), "crew_id":self.config.getUserId(), "schedule_date":self.config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(SUPPLY.self, from: data)
                DispatchQueue.main.async { [self] in
                    let lists = decode.list
                    for item in lists {
                        let vhfRdoNo = item.VHF_RDO_NO ?? ""
                        let ttrRdoNo = item.TTR_RDO_NO ?? ""
                        let astrRdoNo = item.ASTR_RDO_NO ?? ""
                        let lterRdoNo = item.LTER_RDO_NO ?? ""
                        let aodTmtNo = item.AOD_TMT_NO ?? ""
                        let bkeyNo = item.BKEY_NO ?? ""
                        let mtitNo = item.MTIT_NO ?? ""
                        let fstRegDttm = item.FST_REG_DTTM ?? ""
                        
                        if vhfRdoNo != "" {
                            arrayOfSupply.append(SUPPLY_REGIST(code: "01", name: "VHF무전기", serial_no: vhfRdoNo, reg_date: util.convertFormattedDate(date: fstRegDttm, containYear: true, containTime: false)))
                        }
                        if ttrRdoNo != "" {
                            arrayOfSupply.append(SUPPLY_REGIST(code: "02", name: "TETRA무전기", serial_no: ttrRdoNo, reg_date: util.convertFormattedDate(date: fstRegDttm, containYear: true, containTime: false)))
                        }
                        if astrRdoNo != "" {
                            arrayOfSupply.append(SUPPLY_REGIST(code: "03", name: "ASTRO무전기", serial_no: astrRdoNo, reg_date: util.convertFormattedDate(date: fstRegDttm, containYear: true, containTime: false)))
                        }
                        if lterRdoNo != "" {
                            arrayOfSupply.append(SUPPLY_REGIST(code: "04", name: "LTE-R무전기", serial_no: lterRdoNo, reg_date: util.convertFormattedDate(date: fstRegDttm, containYear: true, containTime: false)))
                        }
                        if aodTmtNo != "" {
                            arrayOfSupply.append(SUPPLY_REGIST(code: "05", name: "차축발열감지온도계", serial_no: aodTmtNo, reg_date: util.convertFormattedDate(date: fstRegDttm, containYear: true, containTime: false)))
                        }
                        if bkeyNo != "" {
                            arrayOfSupply.append(SUPPLY_REGIST(code: "06", name: "사각키", serial_no: bkeyNo, reg_date: util.convertFormattedDate(date: fstRegDttm, containYear: true, containTime: false)))
                        }
                        if mtitNo != "" {
                            arrayOfSupply.append(SUPPLY_REGIST(code: "07", name: "MTIT/휴대폰", serial_no: mtitNo, reg_date: util.convertFormattedDate(date: fstRegDttm, containYear: true, containTime: false)))
                        }
                    }
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func setSupply(key: String, value: String) async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "set_equipment", "access_key":self.config.getAccessKey(), "user_id":self.config.getUserId(), "crew_id":self.config.getUserId(), "schedule_date":self.config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "dia_plan_date2":self.config.getPjtDt(), "train":self.config.getSelectedTrainNo(), "key":key, "value":value]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func removeSupply(key: String, value: String) async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "remove_equipment", "access_key":self.config.getAccessKey(), "user_id":self.config.getUserId(), "crew_id":self.config.getUserId(), "schedule_date":self.config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "dia_plan_date2":self.config.getPjtDt(), "train":self.config.getSelectedTrainNo(), "key":key, "value":value]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func getDriver() async{
        let semaphore = DispatchSemaphore(value: 0)
        arrayOfDriver.removeAll()
        let dictionary = ["order": "get_driver", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "train":config.getSelectedTrainNo()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(DRIVERS.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfDrivers = decode.list
                    for driver in arrayOfDrivers {
                        let trnNo = driver.TRN_NO ?? ""
                        let empName = driver.EMP_NM ?? ""
                        let dptStn = driver.DPT_STN_NM ?? ""
                        let arvStn = driver.ARV_STN_NM ?? ""
                        
                        arrayOfDriver.append(trnNo + ", " + empName + "(" + dptStn + ">" + arvStn + ")")
                    }
                    semaphore.signal()
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
        semaphore.wait()
    }
    
    func getCheckRadio() {
        arrayOfCheckRadio.removeAll()
        let dictionary = ["order": "get_check_radio", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":self.config.getTrainDate(), "train_no":self.config.getSelectedTrainNo()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CHECK_RADIO_LIST.self, from: data)
                DispatchQueue.main.async { [self] in
                    for list in decode.list {
                        let trainNo = (list.TRN_NO ?? "").replacingOccurrences(of: " ", with: "")
                        let result = (list.RMK_1_CONT ?? "").replacingOccurrences(of: " ", with: "")
                        if trainNo == config.getSelectedTrainNo() {
                            if result != "" {
                                self.arrayOfCheckRadio.append(list)
                            }                            
                        }
                    }
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func setCheckRaio(radioInfo: [RADIO_INFO]) async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "set_check_radio", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "work_days":self.config.getWorkDays(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train_no":config.getSelectedTrainNo(), "car_no":radioInfo[0].CAR_NO, "driver_id":radioInfo[0].DRIVER_ID, "driver_dept":radioInfo[0].DRIVER_DEPT, "driver_car_type":radioInfo[0].CAR_TYPE, "name":radioInfo[0].NAME, "phone":radioInfo[0].PHONE, "depart_code":radioInfo[0].DEPART_CODE, "arrive_code":radioInfo[0].ARRIVE_CODE, "train_cno":radioInfo[0].TRAIN_CNO, "train_length":radioInfo[0].TRAIN_LEN, "depart_time":radioInfo[0].DEPART_TIME, "check_msg":radioInfo[0].MSG]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)        
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func removeCheckRadio(carNo: String, seq: String, driverId: String) async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "remove_check_radio", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "work_days":self.config.getWorkDays(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train_no":config.getSelectedTrainNo(), "car_no":carNo, "seq":seq, "driver_id":driverId]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func getCarNo() async {
        arrayOfCarNo.removeAll()
        arrayOfCarModel.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "get_car_series", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "train":config.getSelectedTrainNo()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CAR_SERIES.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfCarNo = decode.list
                    for list in arrayOfCarNo {
                        self.arrayOfCarModel.append(CAR_MODEL(car: list.CAR_NO ?? "", checked: false))
                    }
                    
                }
                semaphore.signal()
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
        semaphore.wait()
    }
    
    func getEquipCheckList() {
        let dictionary = ["order": "base_code", "code":"CR0121", "access_key":self.config.getAccessKey()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CODE.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfEquipCheckList = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)                
            }
        }.resume()
    }
    
    func getEquipFixList() {
        let dictionary = ["order": "base_code", "code":"CR0122", "access_key":self.config.getAccessKey()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CODE.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfEquipFixList = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
            }
        }.resume()
    }
    
    func getCheckEquipment() {
        let dictionary = ["order": "get_check_equipment", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train_no":config.getSelectedTrainNo()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CHECK_EQUIPMENT_LIST.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfCheckEquipment = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func setCheckEquipment(carNo: String, equipCode: String, actionCode: String, msg: String) async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "set_check_equipment", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train_no":config.getSelectedTrainNo(), "car_no":carNo, "equipment_code":equipCode, "action_code":actionCode, "etc1":"", "etc2":"", "seq":"1", "check_message":msg]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func removeCheckEquipment(carNo: String, equipCode: String) async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "remove_check_equipment", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "work_days":self.config.getWorkDays(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train_no":config.getSelectedTrainNo(), "car_no":carNo, "seq":"1", "equipment_code":equipCode]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func getCarCheck() {
        let dictionary = ["order": "get_car_check", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train":config.getSelectedTrainNo()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CHECK_CAR_LIST.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfCarCheck = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func setCarCheck(checkCode: String, check_msg: String) async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)        
        let dictionary = ["order": "set_car_check", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "work_days":self.config.getWorkDays(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train":config.getSelectedTrainNo(), "check_code":checkCode, "check_action":check_msg, "remark1":"", "remark2":""]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func removeCarCheck(seq: String) async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "remove_car_check", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "work_days":self.config.getWorkDays(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train":config.getSelectedTrainNo(), "seq":seq]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func getCarFaultMainCode() {
        let dictionary = ["order": "base_code", "code":"PM0015", "access_key":self.config.getAccessKey(), "crew_plan_code":self.config.getCrewPlanCd()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CODE.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfFaultMainCode = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func getCarFaultMidCode(mainCode: String) {
        let dictionary = ["order": "sub_code", "code":"PM0016", "access_key":self.config.getAccessKey(), "crew_plan_code":self.config.getCrewPlanCd(), "sub":mainCode]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CODE.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfFaultMidCode = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
            }
        }.resume()
    }
    
    func getCarFault() {
        let dictionary = ["order": "get_car_fault", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getTrainDate(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train_no":config.getSelectedTrainNo()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CAR_FAULT_LIST.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfCarFault = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func getDestination() {
        let dictionary = ["order": "get_stop_info", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "train_no":config.getSelectedTrainNo()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(TRAIN_STATION_LIST.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfTrainStation = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func setCarFault(carNo: String, faultCode: String, faultMsg: String, destination: String) async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "set_car_fault", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getTrainDate(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "work_days":self.config.getWorkDays(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train_no":config.getSelectedTrainNo(), "car_no":carNo, "fault_code":faultCode, "fault_message":faultMsg, "arrive_station":destination]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func removeCarFault(carNo: String, faultCode: String, faultMsg: String, destination: String, seq: String) async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "remove_car_fault", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getTrainDate(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "work_days":self.config.getWorkDays(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train_no":config.getSelectedTrainNo(), "car_no":carNo, "fault_code":faultCode, "fault_message":faultMsg, "arrive_station":destination, "seq":seq]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func getBroadMainCode() {
        let dictionary = ["order": "get_broad_code", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "train":config.getSelectedTrainNo(), "code":""]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CODE.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfBroadMainCode = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func getBroadMidCode(mainCode: String) async {
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "get_broad_code", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "train":config.getSelectedTrainNo(), "code":mainCode]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CODE.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfBroadMidCode = decode.list
                }
                semaphore.signal()
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
        semaphore.wait()
    }
    
    func getBroadSubCode(midCode: String) async {
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "get_broad_code", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "train":config.getSelectedTrainNo(), "code":midCode]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(CODE.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfBroadSubCode = decode.list
                }
                semaphore.signal()
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
        semaphore.wait()
    }
    
    func getBroadMsg(subCode: String) {
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "get_broad_msg", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "code":subCode]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(BROADCAST_MESSAGE.self, from: data)
                DispatchQueue.main.async { [self] in
                    broadMsg = decode.msg ?? ""
                }
                semaphore.signal()
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
        semaphore.wait()
    }
    
    func getBroadCast() {
        let dictionary = ["order": "get_broadcast", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train":config.getSelectedTrainNo()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(BROADCAST_LIST.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfBroadCast = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func setBroadCast(main: String, mid: String, sub: String, count: Int) async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "set_broadcast", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "train":config.getSelectedTrainNo(), "broad_code1":main, "broad_code2":mid, "broad_code3":sub, "broad_count":String(count), "broad_msg":"", "remark1":"", "remark2":""]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func removeBroadCast(seq: String) async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "remove_broadcast", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "work_days":self.config.getWorkDays(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate(), "schedule_date2":config.getTrainDate(), "train":config.getSelectedTrainNo(),  "seq":seq]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func getNextDia() {
        let dictionary = ["order": "get_next_dia", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            //guard let response_str = String(data: data, encoding: .utf8) else { return }
            do {
                let decode = try JSONDecoder().decode(NEXT_DIA.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfNextDia = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func getNextSchedule(date: String) async {
        let dictionary = ["order": "getSchedule", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(),
                          "crew_id":self.config.getUserId(), "dept_code":self.config.getBlgCd(), "schedule_date":date, "crew_plan_code":self.config.getCrewPlanCd()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            guard let response_str = String(data: data, encoding: .utf8) else { return }
            print("apiGetProject response_str -> \(response_str)")
            do {
                let decode = try JSONDecoder().decode(PROJECT_LIST.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfNextPjt = decode.list
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
    
    func submitData() async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "set_finish_project", "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func cancelData() async -> [WEB_RESULT] {
        arrayOfResult.removeAll()
        let semaphore = DispatchSemaphore(value: 0)
        let dictionary = ["order": "cancel_finish_project", "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd(), "dia_no":self.config.getDiaNo(), "dia_plan_code":self.config.getDiaDvCd(), "dia_plan_date":self.config.getDiaStartDate()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = URLSession(configuration: .default)
        let url = URL(string: Constants().getServcerUrl() + "setMtitData2.jsp")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            do {
                let decode = try JSONDecoder().decode(WEB_RESULT.self, from: data)
                self.arrayOfResult.append(decode)
            } catch(let err) {
                print(err.localizedDescription)
            }
            semaphore.signal()
        }.resume()
        semaphore.wait()
        return arrayOfResult
    }
    
    func getContact() {
        arrayOfContactModel.removeAll()
        let dictionary = ["order": "get_contact", "user_id":self.config.getUserId(), "access_key":self.config.getAccessKey(), "crew_id":self.config.getUserId(), "schedule_date":config.getPjtDt(), "dept_code":self.config.getBlgCd(), "crew_plan_code":self.config.getCrewPlanCd()]
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        let postData = "p="+jsonString!
        
        let session = getSession(second: 10)
        guard let url = URL(string: Constants().getServcerUrl() + "getMtitData2.jsp") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = postData.data(using: .utf8)
        
        session.dataTask(with: request) { (rData, _, _) in
            guard let data = rData else { return }
            guard let response_str = String(data: data, encoding: .utf8) else { return }
            print("apiGetProject response_str -> \(response_str)")
            do {
                let decode = try JSONDecoder().decode(CONTACT_LIST.self, from: data)
                DispatchQueue.main.async { [self] in
                    self.arrayOfContact = decode.list
                    for list in arrayOfContact {
                        let deptName = list.DTL_CD_AVVR_NM ?? ""
                        let position = list.DTL_CD_FOML_NM ?? ""
                        let phoneNo = list.DTL_CD_ENGM_AVVR_NM ?? ""
                        
                        self.arrayOfContactModel.append(CONTACT_MODEL(id: 0, dept_name: deptName, position: position, phone_no: phoneNo, checked: false))
                    }
                }
            } catch(let err) {
                print(err.localizedDescription)
                
            }
        }.resume()
    }
}

