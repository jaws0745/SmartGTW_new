//
//  CryptAES.swift
//  mobilecertifier
//
//  Created by 김준선 on 2018. 10. 31..
//  Copyright © 2018년 김준선. All rights reserved.
//

import Foundation
//import CryptoSwift

class CryptAES {
    
    //let key : Array<UInt8> = [0x03, 0x0e, 0x0f, 0x09, 0x02, 0x06, 0x05, 0x03, 0x05, 0x08,
    //                         0x09, 0x07, 0x09, 0x03, 0x02, 0x03]
    //var iv : Array<UInt8> = [0x03, 0x0e, 0x0f, 0x09, 0x02, 0x06, 0x05, 0x03, 0x05, 0x08,
    //                          0x09, 0x07, 0x09, 0x03, 0x02, 0x03]
    
    var key = ""
    var iv = ""
    
    func getKey() -> String {
        return key
        //return key.substring(to: key.index(key.startIndex, offsetBy: 16))
    }
    
    func getIV(key: Array<UInt8>) -> Array<UInt8> {
        return key
        //return key.substring(to: key.index(key.startIndex, offsetBy: 16))
    }
    
    func setKey(k: String) {
        
        key = k
        let characterArray = Array(k)
        var aesKey: String = ""
        
        aesKey += charToString(ch: [characterArray[11]])
        aesKey += charToString(ch: [characterArray[12]])
        aesKey += charToString(ch: [characterArray[13]])
        aesKey += charToString(ch: [characterArray[14]])
        aesKey += charToString(ch: [characterArray[15]])
        aesKey += charToString(ch: [characterArray[0]])
        aesKey += charToString(ch: [characterArray[1]])
        aesKey += charToString(ch: [characterArray[2]])
        aesKey += charToString(ch: [characterArray[9]])
        aesKey += charToString(ch: [characterArray[10]])
        aesKey += charToString(ch: [characterArray[3]])
        aesKey += charToString(ch: [characterArray[4]])
        aesKey += charToString(ch: [characterArray[5]])
        aesKey += charToString(ch: [characterArray[6]])
        aesKey += charToString(ch: [characterArray[7]])
        aesKey += charToString(ch: [characterArray[8]])
        
        iv = aesKey
        //print("iv:\(iv)")
    }
    
    /*func aesEncrypt(cryptData: String) throws -> String{
        let data = cryptData.data(using: String.Encoding.utf8)
        print("konnect key:\(key)")
        print("konnect iv:\(iv)")
        let enc = try AES(key: key, iv: iv, blockMode:.CBC).encrypt(data!.bytes)
        let encData = NSData(bytes: enc, length: Int(enc.count))
        let base64String: String = encData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0));
        let result = String(base64String)
        
        return result
    }
    
    func aesDecrypt(cryptData: String) throws -> String {
        //print("\(cryptData)")
        var decryptedString = ""
        let encryptedData = NSData(base64Encoded: cryptData.replacingOccurrences(of: " ", with: ""), options:[])!
        let count = encryptedData.length / MemoryLayout<UInt8>.size
        var encrypted = [UInt8](repeating: 0, count: count)
        encryptedData.getBytes(&encrypted, length:count * MemoryLayout<UInt8>.size)
        do {
            //print("\(key)")
            //print("\(iv)")
            let decryptedData = try AES(key: key, iv: iv, blockMode: .CBC).decrypt(encrypted)
            //print("\(decryptedData)")
            //if dataWithHexString(hex: decryptedData) == true {
            decryptedString = String(bytes: decryptedData, encoding: String.Encoding.utf8)!
            //} else {
            //    decryptedString = ""
            //}
        }
        catch {
            print("\(error.localizedDescription)")
        }
        
        return decryptedString
    }*/
    
    func dataWithHexString(hex: [UInt8]) -> Bool {
        var result = true
        for i in 0 ..< hex.count {
            if (hex[i] > 127) {
                result = false
                break
            }
        }
        
        return result
    }

    func replaceSpecialCharacter(val: String) -> String {
        return val.replacingOccurrences(of: "+", with: "%2B").replacingOccurrences(of: "=", with: "%3D").replacingOccurrences(of: "/", with: "%2F")
    }
    
    /*func encodeWithSpecialCharacter(val: String) -> String{
        return try! aesEncrypt(cryptData: val).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!.replacingOccurrences(of: "+", with: "%2B").replacingOccurrences(of: "=", with: "%3D").replacingOccurrences(of: "/", with: "%2F")
    }*/
    
    func charToString(ch: [Character]) -> String {
        return String(ch)
    }
}
