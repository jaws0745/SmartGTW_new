//
//  ColorExtension.swift
//  CrewDiary
//
//  Created by 코레일 on 2022/07/27.
//

import SwiftUI

extension Color {
    static let white2 = Color(hex: "#f7f7f7")
    static let white3 = Color(hex: "#e8e8e8")
    static let silver = Color(hex: "#d9dadf")
    static let warmgray2 = Color(hex: "#767676")
    static let brightBlue = Color(hex: "#076aff")
    static let darkPeach = Color(hex: "#df6e6e")
    static let brownish_grey = Color(hex: "#5e5e5e")
}

extension Color {
    init(hex: String) {
        let scanner = Scanner(string: hex)
        _ = scanner.scanString("#")
        
        var rgb: UInt64 = 0
        scanner.scanHexInt64(&rgb)
        
        let r = Double((rgb >> 16) & 0xFF) / 255.0
        let g = Double((rgb >>  8) & 0xFF) / 255.0
        let b = Double((rgb >>  0) & 0xFF) / 255.0
        self.init(red: r, green: g, blue: b)
    }
}
