//
//  DiaView.swift
//  SmartGTW_new
//
//  Created by 코레일 on 2022/08/30.
//

import SwiftUI
import WebKit

struct DiaView: View {
    let config = Config()
    let util = Utility()
    @StateObject private var getApi = RequestAPI.shared
    
    @State var diaTapped: Bool = true
    @State var diaDetailTapped: Bool = false
    @State var timeTableTapped: Bool = false
    @State var showAlert: Bool = false
    
    @State var urlToLoad: String = ""
    @State var trainNo: String = ""
    
    @State var detailTitle: Array<String> = []
    @State var detailContent: Array<String> = []
    
    var body: some View {
        ZStack {
            Color.white2.edgesIgnoringSafeArea(.top)
            VStack(spacing: 0){
                ZStack {
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color.brightBlue, lineWidth: 1)
                        .background(Color.white)
                        .frame(width: 335, height: 48)
                    HStack(spacing: 0) {
                        Button(action: {
                            diaTapped = true
                            diaDetailTapped = false
                            timeTableTapped = false
                        }) {
                            ZStack {
                                Rectangle()
                                    .fill(Color.white)
                                    .cornerRadius(12, corners: [.topLeft, .bottomLeft])
                                    .frame(width: 110, height: 46)
                                VStack {
                                    Text("다이아")
                                        .font(diaTapped  ? .custom("Spoqa Han Sans Neo Bold", size: 16) : .custom("Spoqa Han Sans Neo Regular", size: 16))
                                        .foregroundColor(diaTapped ? Color.brightBlue : Color.warmgray2)
                                }
                            }
                        }
                        Rectangle()
                            .fill(Color.brightBlue)
                            .frame(width:1, height: 48)
                        Button(action: {
                            diaTapped = false
                            diaDetailTapped = true
                            timeTableTapped = false
                        }) {
                            ZStack {
                                Rectangle()
                                    .fill(Color.white)
                                    .cornerRadius(12, corners: [.topLeft, .bottomLeft])
                                    .frame(width: 110, height: 46)
                                VStack {
                                    Text("상세정보")
                                        .font(diaDetailTapped  ? .custom("Spoqa Han Sans Neo Bold", size: 16) : .custom("Spoqa Han Sans Neo Regular", size: 16))
                                        .foregroundColor(diaDetailTapped ? Color.brightBlue : Color.warmgray2)
                                }
                            }
                        }
                        Rectangle()
                            .fill(Color.brightBlue)
                            .frame(width:1, height: 48)
                        Button(action: {
                            diaTapped = false
                            diaDetailTapped = false
                            timeTableTapped = true
                        }) {
                            ZStack {
                                Rectangle()
                                    .fill(Color.white)
                                    .cornerRadius(12, corners: [.topRight, .bottomRight])
                                    .frame(width: 110, height: 46)
                                VStack {
                                    Text("열차시각표")
                                        .font(timeTableTapped  ? .custom("Spoqa Han Sans Neo Bold", size: 16) : .custom("Spoqa Han Sans Neo Regular", size: 16))
                                        .foregroundColor(timeTableTapped ? Color.brightBlue : Color.warmgray2)
                                }
                            }
                        }
                    }
                }
                .cornerRadius(8)
                Rectangle()
                    .fill(Color.silver)
                    .frame(width: 328, height: 1)
                    .padding(.top, 20)
                    .isHidden(timeTableTapped ? true : false)
                
                ZStack {
                    DiaWebView()
                        .isHidden(diaTapped ? false : true)
                        .edgesIgnoringSafeArea(.bottom)
                    
                    List() {
                        getDetailCell()
                        /*ForEach(getApi.arrayOfPjt, id: \.self) { result in
                            Group {
                                getProjectCell(data: result)
                            }
                            .listRowBackground(Color.white)
                        }
                        .listRowInsets(EdgeInsets())*/
                    }
                    .listStyle(PlainListStyle())
                    .isHidden(diaDetailTapped ? false : true)
                    .font(.custom("Spoqa Han Sans Neo Regular", size: 17))
                    //.scrollContentBackground(Color.white)
                    
                    VStack(spacing: 0) {
                        HStack(spacing: 0) {
                            Spacer()
                            Button(action: {
                                showAlert = true
                            }) {
                                ZStack(alignment: .leading) {
                                    RoundedRectangle(cornerRadius: 8)
                                        .strokeBorder(Color.white3, lineWidth: 1)
                                        .background(Color.white)
                                    HStack {
                                        Text(trainNo)
                                            .font(.custom("Spoqa Han Sans Neo Regular", size: 16))
                                            .foregroundColor(Color.black)
                                            .padding(.leading, 18)
                                        Spacer()
                                        Image("ic_arrow_down")
                                            .resizable()
                                            .frame(width: 24, height: 24)
                                            .padding(.trailing, 10)
                                    }
                                }
                            }
                            .cornerRadius(8)
                            .frame(width: 259, height: 40)
                            .confirmationDialog("알림", isPresented: $showAlert) {
                                ForEach(0..<getApi.arrayOfTrains.count, id: \.self) { i in
                                    Button(action: {
                                        trainNo = getApi.arrayOfTrains[i]
                                        let train = String(getApi.arrayOfTrains[i].split(separator: ">")[1])
                                        self.config.setTrainText(trainText: getApi.arrayOfTrains[i])
                                        self.config.setSelectedTrainNo(selectedTrainNo: train.trimmingCharacters(in: .whitespaces))
                                        self.config.setTrainDate(trainDate: getApi.arrayOfTrainDates[i])
                                        getApi.getSchedule()
                                    }) {
                                        Text(getApi.arrayOfTrains[i])
                                    }
                                    Button("취소", role: .cancel) { }
                                }
                            }
                            
                            Button(action: {
                                getApi.getSchedule()
                            }) {
                                Image("btn_refresh")
                                    .resizable()
                                    .frame(width: 52, height: 40)
                            }
                            .padding(.leading, 17)
                            Spacer()
                        }
                        .padding(.bottom, 16)
                        .background(Color.white2)
                        HStack(spacing: 0) {
                            Text("역명(타는곳)")
                                .padding(.leading, 20)
                            Text("열차위치")
                                .padding(.leading, 20)
                            Text("도착시각")
                                .padding(.leading, 40)
                            Spacer()
                            Text("출발시각")
                                .padding(.trailing, 27)
                        }
                        .frame(width: .infinity, height: 60)
                        .background(Color.white)
                        .font(.custom("Spoqa Han Sans Neo Regular", size: 15))
                        .foregroundColor(Color.warmgray2)
                        
                        Rectangle()
                            .fill(Color.silver)
                            .frame(width: 328, height: 1)
                        
                        List() {
                            ForEach(0..<getApi.arrayOfTimeTable.count, id: \.self) { i in
                                Group {
                                    getTimeTableCell(data: getApi.arrayOfTimeTable[i], idx: i)
                                }
                            }
                            .listRowInsets(EdgeInsets())
                            .listRowSeparator(.hidden)
                        }
                        .listStyle(PlainListStyle())
                        //.scrollContentBackground(Color.white)
                    }
                    .isHidden(timeTableTapped ? false : true)
                }
                .background(Color.white)
            }
            .padding(.top, 20)
        }
        .navigationBarTitle("승무사업")
        .onAppear {
            getApi.getSchedule()
            trainNo = self.config.getTrainText()
        }
    }
    
    func getDetailCell() -> some View {
        return Group {
            Group {
                HStack {
                    Text("다이아")
                        .frame(width: 60, alignment: .leading)
                    Text(config.getDiaNo())
                        .padding(.leading, 15)
                }
                HStack {
                    Text("사업일자")
                        .frame(width: 60, alignment: .leading)
                    Text(util.getReturnDate(date: config.getPjtDt()))
                        .padding(.leading, 15)
                }
                HStack {
                    let gtwTm = util.getWorkTime(time: config.getGWK_tm())
                    let loiwTm = util.getWorkTime(time: config.getLOIW_tm())
                    let totTm = util.getTotTime(time: config.getTotTm())
                    let gtwLoiwTm = gtwTm+"-"+loiwTm+" ("+totTm+")"
                    Text("출퇴근")
                        .frame(width: 60, alignment: .leading)
                    Text(gtwLoiwTm)
                        .padding(.leading, 15)
                }
                HStack {
                    Text("거리")
                        .frame(width: 60, alignment: .leading)
                    Text(util.getDst(dst: config.getTotDst())+"km")
                        .padding(.leading, 15)
                }
                HStack {
                    Text("승무원")
                        .frame(width: 60, alignment: .leading)
                    Text(config.getDiaNo())
                }
                HStack {
                    Text("운전")
                        .frame(width: 60, alignment: .leading)
                    Text(config.getDiaNo())
                }
                HStack {
                    Text("편승")
                        .frame(width: 60, alignment: .leading)
                    Text(config.getDiaNo())
                }
                HStack {
                    Text("준비")
                        .frame(width: 60, alignment: .leading)
                    Text(config.getDiaNo())
                }
                HStack {
                    Text("정리")
                        .frame(width: 60, alignment: .leading)
                    Text(config.getDiaNo())
                }
            }
            Group {
                HStack {
                    Text("감시")
                        .frame(width: 60, alignment: .leading)
                    Text(config.getDiaNo())
                }
                HStack {
                    Text("비상")
                        .frame(width: 60, alignment: .leading)
                    Text(config.getDiaNo())
                }
                HStack {
                    Text("승계")
                        .frame(width: 60, alignment: .leading)
                    Text(config.getDiaNo())
                }
                HStack {
                    Text("휴게")
                        .frame(width: 60, alignment: .leading)
                    Text(config.getDiaNo())
                }
            }
        }
        .font(.custom("Spoqa Han Sans Neo Regular", size: 16))
        .foregroundColor(Color.black)
        .padding(.leading, 10)
    }
    
    func getProjectCell(data: PROJECT) -> some View {
        var flag = ""
        
        if data.GTWC_FLG == "Y" {
            flag = "(出)"
        }
        else if data.ITMT_GTWC_FLG == "Y" {
            flag = "(출)"
        }
        else {
            flag = ""
        }
        
        if data.PJT_HR_DV_NM == "승무" || data.PJT_HR_DV_NM == "운전" {
            return HStack {
                HStack(spacing: 0) {
                    Text(data.PJT_HR_DV_NM ?? "")
                    Text(flag)
                }
                .frame(width: 90, alignment: .leading)
                .padding(.leading, 20)
                .foregroundColor(Color.black)
                VStack(alignment: .leading, spacing: 0) {
                    HStack(spacing: 0) {
                        Text(util.getPjtTime(time: data.DPT_TM ?? "")+" "+data.DPT_STN_NM!)
                        Text("→")
                            .padding(.leading, 3)
                        Text(util.getPjtTime(time: data.ARV_TM ?? "")+" "+data.ARV_STN_NM!)
                            .padding(.leading, 3)
                        Text("("+util.getTotTime(time: data.PJT_TNUM ?? "")+")")
                            .padding(.leading, 3)
                    }
                    HStack(spacing: 0) {
                        Text(data.TRN_CLSF_NM ?? "")
                        Text(data.TRN_NO ?? "")
                            .padding(.leading, 3)
                        Text("("+util.getDst(dst: data.PJT_DST ?? "")+")")
                            .padding(.leading, 3)
                    }
                    .frame(height: .infinity, alignment: .leading)
                }
                .frame(width: .infinity, height: 60, alignment: .leading)
                .padding(.leading, 5)
                .foregroundColor(Color.brightBlue)
            }
            .listRowBackground(Color.white)
        }
        else {
            return HStack {
                HStack(spacing: 0) {
                    Text(data.PJT_HR_DV_NM ?? "")
                    Text(flag)
                }
                .frame(width: 90, alignment: .leading)
                .padding(.leading, 20)
                .foregroundColor(Color.black)
                VStack(spacing: 0) {
                    HStack(spacing: 0) {
                        Text(util.getPjtTime(time: data.DPT_TM ?? "")+" - "+util.getPjtTime(time: data.ARV_TM ?? ""))
                        Text("("+util.getTotTime(time: data.PJT_TNUM ?? "")+")")
                            .padding(.leading, 3)
                        Text("")
                            .padding(.leading, 3)
                        Text("")
                            .padding(.leading, 3)
                    }
                    HStack {
                        Text("")
                        Text("")
                            .padding(.leading, 3)
                        Text("")
                            .padding(.leading, 3)
                    }
                    .frame(height: 0)
                }
                .frame(width: .infinity, height: 40, alignment: .leading)
                .padding(.leading, 5)
                .foregroundColor(Color.black)
            }
            .listRowBackground(Color.white)
        }
    }
    
    func getTimeTableCell(data: TRAIN_SCHEDULE_ITEM, idx: Int) -> some View {
        return HStack {
            let plfNo = data.PLF_NO ?? ""
            let planArrival = util.getFormattedTime(time: data.TRN_PLN_ARV_DTTM, bk: false)
            let actualArrival = util.getFormattedTime(time: data.TRN_ACT_ARV_DTTM, bk: true)
            let planDepart = util.getFormattedTime(time: data.TRN_PLN_DPT_DTTM, bk: false)
            let actualDepart = util.getFormattedTime(time: data.TRN_ACT_DPT_DTTM, bk: true)
            
            Text(data.STN_NM+"("+plfNo.replacingOccurrences(of: " ", with: "")+")")
                .padding(.leading, 20)
                .frame(width: 100, alignment: .leading)
            ZStack {
                Circle()
                    .frame(width:8, height: 8)
                    .foregroundColor(Color.brightBlue)
                if idx == 0 {
                    ZStack {
                        Rectangle()
                            .frame(width: 2, height: 26)
                            .foregroundColor(Color.brightBlue)
                            .padding(.top, 26)
                        if actualDepart == "" {
                            Image("ic_train_top")
                                .resizable()
                                .frame(width:26, height:13)
                                .padding(.bottom, 13)
                            Image("ic_train_bottom")
                                .resizable()
                                .frame(width:26, height:13)
                                .padding(.top, 13)
                        }
                        else {
                            if getApi.arrayOfTimeTable.count > 0 {
                                if util.getFormattedTime(time: getApi.arrayOfTimeTable[idx+1].TRN_ACT_ARV_DTTM, bk: false) == "" {
                                    Image("ic_train_top")
                                        .resizable()
                                        .frame(width:26, height:13)
                                        .padding(.top, 39)
                                }
                            }
                        }
                    }
                }
                else if idx == getApi.arrayOfTimeTable.count - 1 {
                    ZStack {
                        Rectangle()
                            .frame(width: 2, height: 26)
                            .foregroundColor(Color.brightBlue)
                            .padding(.bottom, 26)
                        if actualArrival == "" {
                            if util.getFormattedTime(time: getApi.arrayOfTimeTable[idx-1].TRN_ACT_ARV_DTTM, bk: false) != "" {
                                Image("ic_train_bottom")
                                    .resizable()
                                    .frame(width:26, height:13, alignment: .top)
                                    .padding(.bottom, 39)
                            }
                        }
                        else {
                            Image("ic_train_top")
                                .resizable()
                                .frame(width:26, height:13)
                                .padding(.bottom, 13)
                            Image("ic_train_bottom")
                                .resizable()
                                .frame(width:26, height:13)
                                .padding(.top, 13)
                        }
                    }
                }
                else {
                    ZStack {
                        Rectangle()
                            .frame(width: 2, height: .infinity)
                            .foregroundColor(Color.brightBlue)
                        if actualArrival == "" {
                            if util.getFormattedTime(time: getApi.arrayOfTimeTable[idx-1].TRN_ACT_DPT_DTTM, bk: false) != "" {
                                Image("ic_train_bottom")
                                    .resizable()
                                    .frame(width:26, height:13, alignment: .top)
                                    .padding(.bottom, 39)
                            }
                        }
                        else {
                            if actualDepart == "" {
                                Image("ic_train_top")
                                    .resizable()
                                    .frame(width:26, height:13)
                                    .padding(.bottom, 13)
                                Image("ic_train_bottom")
                                    .resizable()
                                    .frame(width:26, height:13)
                                    .padding(.top, 13)
                            }
                            else if util.getFormattedTime(time: getApi.arrayOfTimeTable[idx+1].TRN_ACT_ARV_DTTM, bk: false) == "" {
                                Image("ic_train_top")
                                    .resizable()
                                    .frame(width:26, height:13)
                                    .padding(.top, 39)
                            }
                        }
                    }
                    .frame(height: .infinity)
                }
            }
            .frame(width: 52)
            .padding(.leading, 10)
            
            Spacer()
            VStack {
                Text(planArrival)
                Text(actualArrival)
                    .foregroundColor(Color.darkPeach)
                    .font(.custom("Spoqa Han Sans Neo Regular", size: 13))
            }
            Spacer()
            VStack {
                Text(planDepart)
                Text(actualDepart)
                    .foregroundColor(Color.darkPeach)
                    .font(.custom("Spoqa Han Sans Neo Regular", size: 13))
            }
            .padding(.trailing, 20)
        }
        .foregroundColor(Color.black)
        .font(.custom("Spoqa Han Sans Neo Regular", size: 17))
        .frame(height: 52)
        .listRowBackground(Color.white)
    }
}

struct DiaWebView: UIViewRepresentable {
    let config = Config()
    
    //ui view 만들기
    func makeUIView(context: Context) -> WKWebView {

        guard let url = URL(string: Constants().getDiaUrl() + config.getDiaUrl()) else {
            return WKWebView()
        }
        
        //웹뷰 인스턴스 생성
        let webView = WKWebView()
        
        //웹뷰를 로드한다
        webView.load(URLRequest(url: url))
        webView.scrollView.setZoomScale(2.0, animated: true)
        return webView
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        
    }
}



struct DiaView_Previews: PreviewProvider {
    static var previews: some View {
        DiaView()
    }
}
