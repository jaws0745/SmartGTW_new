//
//  Model.swift
//  MtitCrew
//
//  Created by 코레일 on 2021/04/21.
//

import Foundation

struct LOGIN: Codable {
    let result: String
    let descript: String
    let user_id: String
    let access_key: String
    let check_version: String
}

struct WEB_RESULT: Codable {
    let result: String
    let descript: String
}

struct TITLE_IMAGES: Codable {
    let oid : String
}

struct PRE_SCHEDULE: Codable {
    let result : String?
    let TOT_WRK_TNUM : String?
    let BLG_NM : String?
    let PDIA_NO : String?
    let PDIA_EFC_ST_DT : String?
    let descript : String?
    let TOT_PJT_DST : String?
    let CRW_PLN_DV_CD : String?
    let PJT_DT : String?
    let ALC_SPEC_DV_CD : String?
    let LOIW_TM : String?
    let GWK_TM : String?
    let ND_DNO : String?
    let BLG_CD : String?
    let CRW_PLN_NM : String?
    let PDIA_DV_CD : String?
    let EMP_NM : String?
    let EPNO : String?
}

struct PROJECT_LIST: Codable {
    let result : String
    let descript : String
    let list : [PROJECT]
}

struct PROJECT: Codable, Hashable {
    let TRN_CLSF_NM : String?
    let DPT_TM : String?
    let PJT_TNUM : String?
    let ARV_STN_NM : String?
    let OP_SQNO : String?
    let TRN_NO : String?
    let DPT_STN_NM : String?
    let ANX_TGT_TRN : String?
    let TRN_RUN_DT : String?
    let ARV_TM : String?
    let PJT_DST : String?
    let REP_TRN_FLG : String?
    let GTWC_FLG : String?
    let ITMT_GTWC_FLG : String?
    let PJT_HR_DV_NM : String?
}

struct TRAIN_SCHEDULE: Codable {
    let result: String
    let descript: String
    let list: [TRAIN_SCHEDULE_ITEM]
}

struct TRAIN_SCHEDULE_ITEM: Codable, Hashable {
    let DPT_ARV_DEAL_NM: String
    let STN_NM: String
    let TRN_ACT_ARV_DTTM: String
    let TRN_ACT_DPT_DTTM: String
    let TRN_PLN_ARV_DTTM: String
    let TRN_PLN_DPT_DTTM: String
    let PLF_NO: String?
}

struct CODE: Codable {
    let result: String
    let descript: String
    let list: [CODE_ITEM]
}

struct CODE_ITEM: Codable, Hashable {
    let code: String
    let code_name: String
}

struct NOTICE: Codable {
    let result: String
    let descript: String?
    let list: [NOTICE_ITEM]
}

struct NOTICE_ITEM: Codable {
    let DTCD_REG_FLG: String?
    let STN_ED_KJ: String?
    let BLG_NM: String?
    let RE_REG_FLG: String?
    let PWFL_SEG_ST_SBST_CD: String?
    let PWFL_SEG_CLS_SBST_CD: String?
    let EFC_SEG_ST_KJ: String?
    let LN_KND_CD: String?
    let TRN_NO: String?
    let EFC_SEG_CLS_KJ: String?
    let RUN_TRM_SQNO: String?
    let TMP_PLN_NO: String?
    let DRV_LMT_TRM_DV_CD: String?
    let STN_ST_KJ: String?
    let CHK: String?
    let BLG_CD: String?
    let EFC_SEG_ST_STN_CD: String?
    let ORD_SAOD_DPCH_DT: String?
    let LN_KND_NM: String?
    let KROIS_ARV_STN_CD: String?
    let UP_DN_DV_CD: String?
    let EFC_SEG_CLS_STN_CD: String?
    let EFC_CONT: String?
    let EFC_TRM_CLS_TM: String?
    let SORT_ORDR: String?
    let EFC_ATCL_RSN_CONT: String?
    let ORD_SAOD_KND_CD: String?
    let EFC_SEG_ST_STN_NM: String?
    let EFC_TRM_CLS_DT: String?
    let SELF_REG_FLG: String?
    let ORD_SAOD_KND_NM: String?
    let EFC_SEG_CLS_STN_NM: String?
    let DRV_LMT_TRK_NO: String?
    let NXDY_FLG: String?
    let EFC_TRM_ST_TM: String?
    let KROIS_DPT_STN_CD: String?
    let DRV_LMT_TRM_DV_NM: String?
    let EFC_TRM_ST_DT: String?
}

struct SUPPLY: Codable {
    let result: String
    let descript: String
    let list: [SUPPLY_ITEM]
}

struct SUPPLY_ITEM: Codable {
    let DRC_ATCL_CFM_DTTM: String?
    let VHF_RDO_NO: String?
    let FST_REG_PS_EPNO: String?
    let PDIA_NO: String?
    let PDIA_EFC_ST_DT: String?
    let ASTR_RDO_NO: String?
    let LAST_CHG_PS_EPNO: String?
    let CRW_PLN_DV_CD: String?
    let PJT_DT: String?
    let ETC_1_NO: String?
    let LAST_CHG_DTTM: String?
    let ETC_2_NO: String?
    let BLG_CD: String?
    let PDIA_DV_CD: String?
    let BKEY_NO: String?
    let FST_REG_DTTM: String?
    let TTR_RDO_NO: String?
    let AOD_TMT_NO: String?
    let ETC_3_NO: String?
    let EPNO: String?
    let LTER_RDO_NO: String?
    let MTIT_NO: String?
}

struct SUPPLY_REGIST: Codable, Hashable {
    let code: String
    let name: String
    let serial_no: String
    let reg_date: String
}

struct CONTACT: Codable {
    let oid: String
    let dept: String
    let position: String
    let phone: String
    let checked: String
}

struct CAR_SERIES: Codable {
    let result: String
    let descript: String
    let list: [CARS]
}

struct CARS: Codable, Hashable {
    let CAR_NO: String?
    let CAR_CPL_ORDR: String?
    let TRN_RUN_DT: String?
    let GUBUN: String?
    let TRN_NO: String?
    let TCLS_AVVR_CD: String?
    let CAR_GU_NM: String?
}

struct DRIVERS: Codable {
    let result: String
    let descript: String
    let list: [DRIVER]
}

struct DRIVER: Codable {
    let TRN_NO: String?
    let EMP_NM: String?
    let DPT_STN_NM: String?
    let ARV_STN_NM: String?
    let EPNO: String?
    let CAR_NO: String?
    let BLG_CD: String?
    let CRW_PLN_DV_CD: String?
    let HNDY_TELN: String?
    let DPT_STN_CD: String?
    let ARV_STN_CD: String?
    let TRN_LEN: String?
    let DPT_TM: String?
    let TRN_CNO: String?
}

struct RADIO_INFO: Codable {
    let CAR_NO: String?
    let DRIVER_ID: String?
    let DRIVER_DEPT: String?
    let CAR_TYPE: String?
    let NAME: String?
    let PHONE: String?
    let DEPART_CODE: String?
    let ARRIVE_CODE: String?
    let TRAIN_CNO: String?
    let TRAIN_LEN: String?
    let DEPART_TIME: String?
    let MSG: String?
}

struct CONTACT_LIST: Codable {
    let result: String
    let descript: String
    let list: [CONTACTS]
}

struct CONTACTS: Codable {
    let DTL_CD_AVVR_NM: String?
    let DTL_CD_ENGM_AVVR_NM: String?
    let DTL_CD_FOML_NM: String?
}

struct CONTACT_DB: Codable {
    let oid: String
    let dept: String
    let position: String
    let phone: String
    var checked: String
}

struct NEXT_DIA: Codable {
    let result: String
    let descript: String
    let list: [DIA_INFO]
}

struct DIA_INFO: Codable, Hashable {
    let TOT_WRK_TNUM: String?
    let NXT_PDIA_NO: String?
    let NXT_GWK_TM: String?
    let PDIA_NO: String?
    let CRW_PLN_DV_CD: String?
    let PJT_DT: String?
    let LOIW_TM: String?
    let NXT_TOT_WRK_TNUM: String?
    let GWK_TM: String?
    let BLG_CD: String?
    let NXT_LOIW_TM: String?
    let EPNO: String?
    let NXT_PJT_DT: String?
}

struct CHECK_RADIO_LIST: Codable {
    let result: String
    let descript: String
    let list: [CHECK_RADIO]
}

struct CHECK_RADIO: Codable, Hashable {
    let FST_REG_PS_EPNO: String?
    let ARV_STN_NM: String?
    let ORG_CRW_PLN_DV_CD: String?
    let PDIA_NO: String?
    let RDO_SSTV_CFM_DTTM: String?
    let TRN_NO: String?
    let DPT_STN_NM: String?
    let TRN_LEN: String?
    let LCM_CRW_EPNO: String?
    let LAST_CHG_DTTM: String?
    let CAR_NO: String?
    let LCM_CRW_BLG_CD: String?
    let TRN_CNO: String?
    let ARV_TM: String?
    let BLG_CD: String?
    let ARV_STN_CD: String?
    let EMP_NM: String?
    let DPT_STN_RUN_ORDR: String?
    let FST_REG_DTTM: String?
    let LCM_CRW_PLN_DV_CD: String?
    let EPNO: String?
    let DPT_STN_CD: String?
    let DPT_TM: String?
    let PDIA_EFC_ST_DT: String?
    let LAST_CHG_PS_EPNO: String?
    let FST_REG_PSORG_EPNO_EPNO: String?
    let CRW_PLN_DV_CD: String?
    let ORG_BLG_CD: String?
    let PJT_DT: String?
    let RMK_1_CONT: String?
    let INP_ORDR: String?
    let TRN_RUN_DT: String?
    let PDIA_DV_CD: String?
    let HNDY_TELN: String?
}

struct CHECK_EQUIPMENT_LIST: Codable {
    let result: String
    let descript: String
    let list: [CHECK_EQUIPMENT]
}

struct CHECK_EQUIPMENT: Codable, Hashable {
    let ACTION_NAME: String?
    let BAD_FLG: String?
    let BLG_CD: String?
    let CAR_NO: String?
    let CRW_PLN_DV_CD: String?
    let EPNO: String?
    let EQUIPMENT_NAME: String?
    let ETC_1_CD: String?
    let ETC_2_CD: String?
    let FST_REG_DTTM: String?
    let FST_REG_PS_EPNO: String?
    let INP_ORDR: String?
    let LAST_CHG_DTTM: String?
    let LAST_CHG_PS_EPNO: String?
    let MSR_CNQE_CD: String?
    let PDIA_DV_CD: String?
    let PDIA_EFC_ST_DT: String?
    let PDIA_NO: String?
    let PJT_DT: String?
    let PSC_EMG_GOD_CD: String?
    let RMK_1_CONT: String?
    let TRN_NO: String?
    let TRN_RUN_DT: String?
}

struct CHECK_CAR_LIST: Codable {
    let result: String
    let descript: String
    let list: [CHECK_CAR]
}

struct CHECK_CAR: Codable {
    let BLG_CD: String?
    let CHECK_ACTION: String?
    let CHECK_CODE: String?
    let CHEK_ATCL_DV_CD: String?
    let CHEK_CONT: String?
    let CRW_PLN_DV_CD: String?
    let EPNO: String?
    let ETC_1_CD: String?
    let ETC_2_CD: String?
    let FST_REG_DTTM: String?
    let FST_REG_PS_EPNO: String?
    let INP_ORDR: String?
    let LAST_CHG_DTTM: String?
    let LAST_CHG_PS_EPNO: String?
    let PDIA_DV_CD: String?
    let PDIA_EFC_ST_DT: String?
    let PDIA_NO: String?
    let PJT_DT: String?
    let RMK_1_CONT: String?
    let RMK_2_CONT: String?
    let TRN_NO: String?
    let TRN_RUN_DT: String?
}

struct CAR_FAULT_LIST: Codable {
    let result: String
    let descript: String
    let list: [CAR_FAULT]
}

struct CAR_FAULT: Codable {
    let ARV_STN_CD: String?
    let BAD_CONT: String?
    let BAD_NTY_PRS_CONT: String?
    let BAD_NTY_PRS_FLG: String?
    let BAD_NTY_SQNO: String?
    let BLG_CD: String?
    let CAR_NO: String?
    let CRW_PLN_DV_CD: String?
    let DTL_CD_FOML_NM: String?
    let FST_REG_DTTM: String?
    let FST_REG_PS_EPNO: String?
    let ISP_BLG_CD: String?
    let LAST_CHG_DTTM: String?
    let LAST_CHG_PS_EPNO: String?
    let PSC_BAD_DUP_FLG: String?
    let PSC_BAD_SPEC_REG_CD: String?
    let RSV_SALE_BAD_NTY_SQNO: String?
    let RUN_DT: String?
    let TNSM_FLG: String?
    let TRN_NO: String?
    let VLID_YM: String?
}

struct BROADCAST_LIST: Codable {
    let result: String
    let descript: String
    let list: [BROADCAST]
}

struct BROADCAST: Codable {
    let BLG_CD: String?
    let BRD_CONT: String?
    let BRD_TNO: String?
    let BROADCAST_CODE: String?
    let CRW_PLN_DV_CD: String?
    let EPNO: String?
    let ETC_1_CD: String?
    let ETC_2_CD: String?
    let FST_REG_DTTM: String?
    let FST_REG_PS_EPNO: String?
    let INP_ORDR: String?
    let LAST_CHG_DTTM: String?
    let LAST_CHG_PS_EPNO: String?
    let LEAD_BRD_LSRT_CD: String?
    let LEAD_BRD_MIDS_CD: String?
    let LEAD_BRD_SSRT_CD: String?
    let PDIA_DV_CD: String?
    let PDIA_EFC_ST_DT: String?
    let PDIA_NO: String?
    let PJT_DT: String?
    let TRN_NO: String?
    let TRN_RUN_DT: String?
}

struct INSTRUCTION_LIST: Codable {
    let result: String
    let descript: String
    let list: [INSTRUCTION]
}

struct INSTRUCTION: Codable, Hashable {
    let DRC_ATCL_CONT: String?
}

struct BROADCAST_MESSAGE: Codable {
    let result: String?
    let descript: String?
    let msg: String?
}

struct TRAIN_STATION_LIST: Codable {
    let result: String
    let descript: String
    let list: [TRAIN_STATION]
}

struct TRAIN_STATION: Codable {
    let DPT_ARV_DEAL_NM: String?
    let STN_CD: String?
    let STN_NM: String?
}

struct FIRST_CLASS: Codable {
    let result: String?
    let train_class_code8: String?
    let descript: String?
    let msg_code: String?
    let supple_train_list: [FXTR_LIST]
    let supple_code_list: [GDCD_LIST]
}

struct FXTR_LIST: Codable {
    let sprmGodsCd: String
    var godsIniQnty: String = "0"
    var godsUseQnty: String = "0"
    var godsIvntQnty: String = "0"
    var godsWdrwQnty: String = "0"
    var cdNm: String?
    var changed: String?
}

struct GDCD_LIST: Codable {
    let sprmGodsCd: String?
    let cdNm: String?
}

struct SET_FIRST: Codable {
    let result: String?
    let descript: String?
    let msg_code: String?
}

struct CONTACT_MODEL : Codable, Hashable, Identifiable {
    let id: Int
    let dept_name: String
    let position: String
    let phone_no: String
    var checked: Bool
}

struct DIAGRAM : Codable {
    let result: String
    let descript: String
    let image_path: String
}

struct CAR_MODEL : Codable, Hashable {
    let car: String
    var checked: Bool
}
