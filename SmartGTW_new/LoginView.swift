//
//  ContentView.swift
//  SmartGTW_new
//
//  Created by 코레일 on 2022/08/29.
//

import SwiftUI
import CryptoKit

struct LoginView: View {
    let vpnProcess = VPNProcess()
    let config = Config()
    let util = Utility()
    
    @State var connectedVPN = false
    @State var userId: String = UserDefaults.standard.string(forKey: "userId") ?? ""
    @State var userPw: String = UserDefaults.standard.string(forKey: "userPw") ?? ""
    @State var isSuccess: Bool = false
    @State var showAlert: Bool = false
    @State var isLoading: Bool = false
    @State var alertMessage: String = ""
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.white.edgesIgnoringSafeArea(.all)
                if isLoading {
                    ProgressView()
                }
                VStack {
                    ZStack(alignment: .leading) {
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(Color.silver, lineWidth: 2)
                            .background(Color.white2)
                            .cornerRadius(8)
                            .frame(width: 312, height: 56)
                        Text(util.getLoginDate())
                            .foregroundColor(Color.warmgray2)
                            .font(.custom("Spoqa Han Sans Neo Regular", size: 16))
                            .padding(16)
                    }
                    .padding(.top, 45)
                    .padding(.bottom, 8)
                    ZStack {
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(Color.silver, lineWidth: 1)
                            .background(Color.white)
                            .frame(width: 312, height: 56)
                        TextField("사번을 입력해주세요", text: $userId)
                            .frame(width: 312, height: 56)
                            .foregroundColor(Color.warmgray2)
                            .font(.custom("Spoqa Han Sans Neo Regular", size: 16))
                            .padding(.leading, 30)
                            .keyboardType(.decimalPad)
                    }
                    .padding(.bottom, 8)
                    ZStack {
                        RoundedRectangle(cornerRadius: 8)
                            .stroke(Color.silver, lineWidth: 1)
                            .background(Color.white)
                            .frame(width: 312, height: 56)
                        SecureField("비밀번호를 입력해주세요", text: $userPw)
                            .frame(width: 312, height: 56)
                            .foregroundColor(Color.warmgray2)
                            .font(.custom("Spoqa Han Sans Neo Regular", size: 16))
                            .padding(.leading, 30)
                    }
                    .padding(.bottom, 15)
                    NavigationLink(destination: MenuView(logoutActive: $isSuccess), isActive: $isSuccess) {
                        Button(action: {
                            hideKeyboard()
                            getLogin()
                            //isSuccess = true
                        }) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 8)
                                    .background(Color.brightBlue)
                                    .frame(width: 312, height: 56)
                                Text("로그인")
                                    .foregroundColor(Color.white)
                                    .font(.custom("Spoqa Han Sans Neo Bold", size: 16))
                            }
                            
                        }
                        .cornerRadius(8)
                        .alert("알림", isPresented: $showAlert) {
                            Button("확인") {
                                isLoading = false
                                //isSuccess = true
                            }
                        } message: {
                            Text(alertMessage)
                        }
                    }
                    Spacer()
                }
            }
            .navigationBarTitleDisplayMode(.inline)
        }
        .onAppear() {
            if checkConnectedVpn() == false {
                connectVPN()
            } else {
                connectedVPN = true
            }
        }
    }
    
    
    // vpn 연결여부 확인
    func checkConnectedVpn() -> Bool {
        var connected = false
        let status:String = self.vpnProcess.checkVpnStatus()

        print("konnect status : \(status)")
        if status == "VPN 연결 안됨" {
            connected = false
        } else {
            connected = true
        }
        
        return connected
    }
    
    // vpn 연결
    func connectVPN() {
        let param = "opmode=2&action=connect&return=MtitCrew&userid=" + config.getUserId() + "_55&password=" + config.getUserPw()
        //print("network: \(param)")
        let urlString: String = "axvpn://?" + param
        
        if let url = URL(string: "\(urlString)"), !url.absoluteString.isEmpty {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func toSHA256(userPw: String) -> String {
        let data = userPw.data(using: .utf8)
        let sha256 = SHA256.hash(data: data!)
        let shaData = sha256.compactMap{String(format: "%02x", $0)}.joined()
        return shaData
    }
    
    // 로그인 처리
    func getLogin() {
        if checkConnectedVpn() == false {
            connectVPN()
        } else {
            isLoading = true
            let dictionary = ["order": "login", "user_id":userId, "user_pw":toSHA256(userPw: userPw), "ver":Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String]
            let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
            let jsonString = String(data: jsonData!, encoding: .utf8)
            
            let postData = "p="+jsonString!
            apiLogin(strUrl: "getMtitData2.jsp", postData: postData, success: { (results) in
                if results!.result == "success" {
                    self.config.setAccessKey(accessKey: results!.access_key)
                    self.config.setIsFirstLogin(firstLogin: true)
                    
                    DispatchQueue.main.async {
                        self.config.setUserId(userId: results!.user_id)
                        UserDefaults.standard.set(results!.user_id, forKey: "userId")
                        self.config.setUserPw(userPw: userPw)
                        UserDefaults.standard.set(userPw, forKey: "userPw")
                        if results!.descript.contains("새로운") {
                            alertMessage = results!.descript
                            showAlert = true
                        } else {
                            isLoading = false
                            isSuccess = true
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        alertMessage = results!.descript
                        showAlert = true
                        
                    }
                }
            }) { (err) in
                print("getTitleImage error \(String(describing: err))")
                DispatchQueue.main.async {
                    alertMessage = "로그인에 실패하였습니다."
                    showAlert = true
                }
            }
        }
    }
}

#if canImport(UIKit)
extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
#endif

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
