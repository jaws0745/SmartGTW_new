//
//  SmartGTW_newApp.swift
//  SmartGTW_new
//
//  Created by 코레일 on 2022/08/29.
//

import SwiftUI

@main
struct SmartGTW_newApp: App {
    var body: some Scene {
        WindowGroup {
            LoginView()
        }
    }
}
