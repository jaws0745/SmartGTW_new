//
//  EfcDeliverView.swift
//  SmartGTW_new
//
//  Created by 코레일 on 2022/09/05.
//

import SwiftUI

struct EfcDeliverView: View {
    let config = Config()
    let util = Utility()
    @StateObject private var getApi = RequestAPI.shared
    
    @State var arrayOfSelectedNotice = [NOTICE_ITEM]()
    
    @State var showAlert: Bool = false
    @State var isSelected: Bool = false
    
    @State var noticeSelectText: String = "명령구분 선택"
    
    var body: some View {
        ZStack {
            Color.white2.edgesIgnoringSafeArea(.top)
            VStack {
                Button(action: {
                    showAlert = true
                }) {
                    ZStack(alignment: .leading) {
                        RoundedRectangle(cornerRadius: 8)
                            .strokeBorder(Color.white3, lineWidth: 1)
                            .background(Color.white)
                        HStack {
                            Text(noticeSelectText)
                                .font(.custom("Spoqa Han Sans Neo Regular", size: 16))
                                .foregroundColor(Color.black)
                                .padding(.leading, 18)
                            Spacer()
                            Image("ic_arrow_down")
                                .resizable()
                                .frame(width: 24, height: 24)
                                .padding(.trailing, 10)
                        }
                    }
                }
                .cornerRadius(8)
                .frame(width: 327, height: 48)
                .padding(.top, 20)
                .confirmationDialog("알림", isPresented: $showAlert) {
                    ForEach(getApi.arrayOfNoticeCode, id: \.self) {result in
                        Button(action: {
                            noticeSelectText = result
                            if noticeSelectText == "전체보기" {
                                isSelected = false
                            }
                            else {
                                isSelected = true
                                self.arrayOfSelectedNotice.removeAll()
                                for i in 0..<getApi.arrayOfNotice.count {
                                    if String(getApi.arrayOfNotice[i].ORD_SAOD_KND_NM ?? "") == noticeSelectText {
                                        arrayOfSelectedNotice.append(getApi.arrayOfNotice[i])
                                    }
                                }
                            }
                        }) {
                            Text(result)
                        }
                        Button("취소", role: .cancel) { }
                    }
                }
                List() {
                    if isSelected == false {
                        ForEach(0..<getApi.arrayOfNotice.count, id: \.self) { i in
                            Group {
                                getNoticeCell(data: getApi.arrayOfNotice[i], idx: i)
                            }
                        }
                        .listRowInsets(EdgeInsets())
                    }
                    else {
                        ForEach(0..<self.arrayOfSelectedNotice.count, id: \.self) { i in
                            Group {
                                getNoticeCell(data: self.arrayOfSelectedNotice[i], idx: i)
                            }
                        }
                        .listRowInsets(EdgeInsets())
                    }
                }
                .listStyle(PlainListStyle())
                //.scrollContentBackground(Color.white)
            }
            .navigationTitle("시행전달부")
            .onAppear() {
                getApi.getNoticeCode()
                getApi.getNotice()
            }
        }
    }
    
    func getNoticeCell(data: NOTICE_ITEM, idx: Int) -> some View {
        return VStack(alignment: .leading, spacing: 5) {
            Text("· 기간시종 : "+util.getMonthDay(date: data.EFC_TRM_ST_DT ?? "")+"~"+util.getMonthDay(date: data.EFC_TRM_CLS_DT ?? ""))
            Text("· 익일구분 : "+(data.DRV_LMT_TRM_DV_NM ?? ""))
            Text("· 시각시종 : "+util.getPjtTime(time: data.EFC_TRM_ST_TM ?? "")+"~"+util.getPjtTime(time: data.EFC_TRM_CLS_TM ?? ""))
            Text("· 역간구간 : "+(data.EFC_SEG_ST_STN_NM ?? "")+"~"+(data.EFC_SEG_CLS_STN_NM ?? ""))
            Text("· 선로구분 : "+(data.LN_KND_NM ?? ""))
            Text("· 지점시종 : "+(data.EFC_SEG_ST_KJ ?? "")+"~"+(data.EFC_SEG_CLS_KJ ?? ""))
            Text("· 사유 : "+(data.EFC_ATCL_RSN_CONT ?? ""))
            Text("· 시행사항 : "+(data.EFC_CONT ?? ""))
        }
        .frame(height: 200)
        .padding(.leading, 20)
        .foregroundColor(Color.black)
        .font(.custom("Spoqa Han Sans Neo Regular", size: 17))
        .listRowBackground(Color.white)
    }
}

struct EfcDeliverView_Previews: PreviewProvider {
    static var previews: some View {
        EfcDeliverView()
    }
}
